import React from 'react';
import { Redirect, Route, Switch } from "react-router-dom";
import LoginPage from "./containers/LoginPage/LoginPage";
import AddNewUser from "./components/AddNewUser/AddNewUser";
import NewTask from "./containers/NewTask/NewTask";
import EditNewsForm from "./components/EditNewsForm/EditNewsForm"
import OneNews from "./components/OneNews/OneNews";
import AllNews from "./containers/AllNews/AllNews";
import AddNewsForm from "./containers/AddNewsForm/AddNewsForm";
import Categories from "./containers/Categories/Categories";
import MainPage from "./containers/MainPage/MainPage";
import AllUsers from "./containers/AllUsers/AllUsers";
import OneUser from "./components/OneUser/OneUser";
import EditUserForm from "./components/EditUserForm/EditUserForm";
import Countdown from "./containers/Countdown/Countdown";
import Diagram from "./containers/Diagram/App";

const ProtectRoute = ({isAllowed, ...props}) => (
	isAllowed ? <Route {...props} /> : <Redirect to="/login"/>
);

const Routes = ({user}) => {
	return (
		<Switch>
			<Route path="/login" exact component={LoginPage}/>
			<ProtectRoute exact isAllowed={user} path="/" component={MainPage}/>
			<ProtectRoute exact isAllowed={user} path="/news_editor/:id" component={EditNewsForm}/>
			<ProtectRoute exact isAllowed={user} path="/one_news/:id" component={OneNews}/>
			<ProtectRoute isAllowed={user && user.role === 'admin'} path="/users" component={AllUsers}/>
			<ProtectRoute isAllowed={user} exact path="/new-task" component={NewTask}/>
			<ProtectRoute isAllowed={user} exact path="/new-task/:id" component={NewTask}/>
			<ProtectRoute isAllowed={user} path="/newses" component={AllNews}/>
			<ProtectRoute isAllowed={user && user.role === 'admin'} path="/one_user/:id" component={OneUser}/>
			<ProtectRoute isAllowed={user && user.role === 'admin'} path="/add-user" component={AddNewUser}/>
			<ProtectRoute isAllowed={user && user.role === 'admin'} path="/user_editor/:id" component={EditUserForm}/>
			<ProtectRoute isAllowed={user && user.role === 'admin'} path="/news-creator" component={AddNewsForm}/>
			<ProtectRoute isAllowed={user && user.role === 'admin'} path="/categories" component={Categories}/>
			<ProtectRoute isAllowed={user && user.role === 'admin'} path="/diagram" component={Diagram}/>
			<ProtectRoute isAllowed={user} path="/countdown/:id" component={Countdown}/>
		</Switch>
	);
};

export default Routes;
