import React, { Component } from 'react';
import { connect } from 'react-redux';
import './OneNews.css';
import config from "../../config";
import notFound from '../../assets/images/not-found.jpeg';
import { getOneNews } from "../../store/actions/newses";
import Layout from "../../components/Layout/Layout";

class OneNews extends Component {
	
	componentDidMount() {
		this.props.getOneNews(this.props.match.params.id);
		
	};
	
	render() {
		let image = notFound;
		
		if (this.props.newses.image) {
			image = config.apiUrl + 'uploads/' + this.props.newses.image;
		}
		
		if (!this.props.newses) {
			return <div>Loading...</div>
		}
		
		return (
			<Layout>
				<div className="content_text_one_news">
					<h3 className="one_news_big_title">Вы читаете новость</h3>
					<div className="one_news_content">
						<div className="one_news_image">
                            <img src={image} alt='news_image'/>
						</div>
						<div className="one_news_info">
                            <div className="one_news_title">
                                <h3>{this.props.newses.title}</h3>
                            </div>
                            <div className="one_news_description">
                                <p>{this.props.newses.description}</p>
                            </div>
						</div>
					</div>
				</div>
			</Layout>
		);
	}
}

const mapStateToProps = state => {
	return {
		newses: state.newses.news
	}
};

const mapDispatchToProps = dispatch => ({
	getOneNews: (id) => dispatch(getOneNews(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(OneNews);