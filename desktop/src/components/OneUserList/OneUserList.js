import React from 'react';
import {Link} from "react-router-dom";
import PropTypes from 'prop-types';
import './OneUserList.css';
import config from '../../config';
import notFound from '../../assets/images/not-found.jpeg';
import {FaTrash} from "react-icons/lib/fa/index";
import {translate} from "../../localization/i18n";
import {MdEdit} from "react-icons/lib/md/index";

class OneUserList extends React.Component {

    handleDeleteClick = (e) => {
        e.preventDefault();
        this.props.onDelete(this.props.id);
    };


    render() {
        const {id, username, image, description} = this.props;
        const imageToShow = image ? config.apiUrl + '/uploads/' + image : notFound;

        return (
            <li className="content_text user">
                <div className="user_link">
                    <Link className="link_to_one_user" to={'/one_user/' + id}>
                        <div className="one_user_list_image_block">
                            <div className="one_user_list_image"
                                 style={{background: `url(${imageToShow}) no-repeat center`, backgroundSize: 'cover'}}/>
                            <h4 className="one-user-list_username">{username}</h4>
                        </div>
                    </Link>
                </div>
                <div className="one-user-list_description">
                    <span>{translate('oneUserList.shortDescription')}</span>
                    <p>
                        {description ? (description.length > 20 ? `${description.substr(0, 20)}...` : description) : null}
                    </p>
                </div>
                <div className="user_actions">
                    <a className="delete_user" htmlFor="file" onClick={this.handleDeleteClick}><FaTrash/></a>
                    <Link className="edit_user" to={'/user_editor/' + id}>
                        <MdEdit/>
                    </Link>
                </div>
            </li>
        );
    }
}

OneUserList.propTypes = {
    id: PropTypes.string.isRequired,
    username: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    image: PropTypes.string,
};


export default OneUserList;