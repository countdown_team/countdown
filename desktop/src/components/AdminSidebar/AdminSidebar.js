import React from 'react';
import { NavLink } from "react-router-dom";
import { translate } from '../../localization/i18n';

const AdminSidebar = () => {


	return (
		<div className="sidebar">
			<div className="status">{translate('sidebar.admin')}</div>
			<div className="sidebar-menu" onClick={this.changeSideBarStatus}>
				<ul className="up">
					<li><NavLink to="/">{translate('sidebar.tasks')}</NavLink></li>
					<li><NavLink to="/newses"><span>{translate('sidebar.news')}</span></NavLink></li>
					<li><NavLink to="/add-user"><span>{translate('sidebar.addUser')}</span></NavLink></li>
					<li><NavLink to="/news-creator"><span>{translate('sidebar.addNews')}</span></NavLink></li>
					<li><NavLink to="/new-task">{translate('sidebar.addTask')}</NavLink></li>
					<li><NavLink to="/categories">{translate('sidebar.categories')}</NavLink></li>
                    <li><NavLink to="/users"><span>{translate('sidebar.allUsers')}</span></NavLink></li>
                    <li><NavLink to="/diagram"><span>{translate('sidebar.diagram')}</span></NavLink></li>
				</ul>
			</div>
		</div>
	);
};

export default AdminSidebar;
