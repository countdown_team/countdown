import React, { Component } from "react";
import { connect } from "react-redux";
import Input from "../Input/Input";
import Button from "../Button/Button";
import { registerUser } from "../../store/actions/users";
import './AddNewUser.css';
import Layout from "../Layout/Layout";
import { translate } from '../../localization/i18n';
import TextArea from "../TextArea/TextArea";
import { FaImage } from "react-icons/lib/fa/index";
import notFound from '../../assets/images/not-found.jpeg';
import Select from "react-select";
import 'react-select/dist/react-select.css';

class AddNewUser extends Component {
	
	state = {
		username: '',
		password: '',
		description: '',
		role: '',
		image: '',
		preview: null
	};
	
	submitUserFormHandler = (event) => {
		event.preventDefault();
		const formData = new FormData();
		Object.keys(this.state).forEach(key => {
			formData.append(key, this.state[key]);
		});
		this.props.registerUser(formData);
	};
	
	imageChangeHandler = event => {
		if (event.target.files && event.target.files[0]) {
			let reader = new FileReader();
			reader.onload = (event) => {
				this.setState({preview: event.target.result});
			};
			reader.readAsDataURL(event.target.files[0]);
		}
		this.setState({
			[event.target.name]: event.target.files[0]
		});
	};
	
	
	inputChangeHandler = event => {
		this.setState({
			[event.target.name]: event.target.value
		});
	};
	
	selectChangeHandler = (name, role) => {
		if (role) {
			this.setState({[name]: role.value});
		} else {
			this.setState({[name]: ''});
		}
	};
	
	
	render() {
		let imageText;
		if (this.state.image) {
			imageText = <div>{this.state.image.name}</div>
		}
		let image = notFound;
		
		const role = [
			{
				value: 'user',
				label: translate('newUser.userRole')
			}, {
				value: 'admin',
				label: translate('newUser.adminRole')
			},
		];
		
		return (
			<Layout>
				<h1 className="add-user_title">{translate('newUser.subtitle')}</h1>
				<form onSubmit={this.submitUserFormHandler}>
					<div className="add_user_image">
						<img src={this.state.preview || image}
						     alt=""/>
					</div>
					<div className="add-user_row row">
						<Input
							value={this.state.username}
							onChange={this.inputChangeHandler}
							name="username"
							type="text"
							labelText={translate('newUser.username')}
						/>
					</div>
					<div className="add-user_row row">
						<Input
							value={this.state.password}
							onChange={this.inputChangeHandler}
							name="password"
							type="password"
							labelText={translate('newUser.password')}
						/>
					</div>
					<div className="add-user_row row">
						<TextArea
							value={this.state.description}
							onChange={this.inputChangeHandler}
							name="description"
							type="text"
							labelText={translate('newUser.description')}
						/>
					</div>
					<div className="add-user_row row">
						<Select
							name="role"
							value={this.state.role}
							onChange={role => this.selectChangeHandler('role', role)}
							options={role}
							required={true}
							className="add-user-select"
							placeholder={translate('newUser.role')}
						/>
					</div>
					<div className="add-user_select add-user">
						<label className="addImage" htmlFor="image"><FaImage/>{translate('newUser.imageText')}</label>
						<input id="image"
						       style={{display: 'none'}}
						       name="image"
						       onChange={this.imageChangeHandler}
						       type="file"
						/>
						{imageText}
					</div>
					{this.props.loginError &&
					<span className="error">{this.props.loginError.error}</span>
					}
					<div className="row">
						<Button btnClass="login-page_btn">{translate('newUser.register')}</Button>
					</div>
				</form>
			</Layout>
		);
	}
}

const mapStateToProps = () => ({});

const mapDispatchToProps = dispatch => ({
	registerUser: userData => dispatch(registerUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(AddNewUser);
