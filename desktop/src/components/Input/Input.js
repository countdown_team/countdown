import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import nanoid from 'nanoid';
import './Input.css';

const Input = ({value, labelText, onChange, type, name, placeholder, required, className, disabled, onClick, readOnly}) => {
	const id = nanoid();
	
	return (
		<div>
			<Fragment>
				<label htmlFor={id}>{labelText}</label>
				<input
					className={className}
					required={required}
					value={value}
					onChange={onChange}
					id={id}
					name={name}
					placeholder={placeholder}
					type={type}
					disabled={disabled}
					onClick={onClick}
					readOnly={readOnly}
				/>
			</Fragment>
		</div>
	);
};

Input.propTypes = {
	value: PropTypes.any.isRequired,
	labelText: PropTypes.string,
	type: PropTypes.string.isRequired,
	name: PropTypes.string,
	placeholder: PropTypes.string,
	onChange: PropTypes.func,
	required: PropTypes.bool,
	className: PropTypes.string,
	disabled: PropTypes.bool,
	onClick: PropTypes.func,
	readOnly: PropTypes.bool
};

export default Input;
