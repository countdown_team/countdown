import React from 'react';
import './Sidebar.css';
import {NavLink} from "react-router-dom";
import { translate } from "../../localization/i18n";

const Sidebar = () => {
	return (
		<div className="sidebar">
			<div className="status">{translate('sidebar.user')}</div>
			<div className="sidebar-menu">
				<ul className="up">
					<li><NavLink to="/">{translate('sidebar.tasks')}</NavLink></li>
					<li><NavLink to="/newses"><span>{translate('sidebar.news')}</span></NavLink></li>
					<li><NavLink to="/new-task">{translate('sidebar.addTask')}</NavLink></li>
				</ul>
			</div>
		</div>
	);
};

export default Sidebar;
