import React, { Component } from 'react';
import { translate } from "../../localization/i18n";
import PropTypes from "prop-types";

export default class MyComponent extends Component {
	
	static propTypes = {
		visible: PropTypes.bool.isRequired,
		deleteHandler: PropTypes.func.isRequired,
		cancelHandler: PropTypes.func.isRequired,
	};
	
	delete = () => {
		this.props.deleteHandler();
		this.props.cancelHandler();
	};
	
	cancel = event => {
		event.stopPropagation();
		this.props.cancelHandler();
	};
	
	render() {
		if (this.props.visible) {
			return (
				<div className="confirmation-modal_backdrop">
					<div className="confirmation-modal">
						<p>{translate('mainPage.taskModal.confirmDelete')}</p>
						<div className="confirmation-modal_buttons">
							<button
								className="confirmation-modal_buttons--delete"
								onClick={this.delete}>
								{translate('mainPage.taskModal.delete')}
							</button>
							<button
								className="confirmation-modal_buttons--cancel"
								onClick={this.cancel}>
								{translate('mainPage.taskModal.cancel')}
							</button>
						</div>
					</div>
				</div>
			);
		} else return null;
	}
}