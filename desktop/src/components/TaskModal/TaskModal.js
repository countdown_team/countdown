import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import {MdDelete, MdEdit, MdClose, MdList, MdThumbUp} from "react-icons/lib/md/index";
import { translate } from "../../localization/i18n";
import fire from '../../assets/images/fire.gif';
import swal from 'sweetalert2';
import './TaskModal.css';
import ConfirmModal from "../ConfirmModal/ConfirmModal";

class TaskModal extends React.Component {
	static propTypes = {
		task: PropTypes.object,
		visibleModal: PropTypes.bool.isRequired,
		hideModalHandler: PropTypes.func.isRequired,
		editTaskHandler: PropTypes.func.isRequired,
		deleteTaskHandler: PropTypes.func.isRequired,
	};
	
	state = {
		visibleConfirm: false
	};
	
	showConfirmModal = event => {
		event.stopPropagation();
		this.setState({visibleConfirm: true});
	};
	
	hideConfirmModal = () => {
		this.setState({visibleConfirm: false});
	};

	goToCountdown = () => {
		const {task, history} = this.props;
		history.push(`countdown/${task._id}`);
	};

	confirmComplete() {
		const {task, editTask} = this.props;
		swal({
			title: 'Вы уверены что выполнили задачу?',
			text: "Обратно перевести задачу в прежнее состояние будет невозможно!",
			type: 'info',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Да!',
			cancelButtonText: 'Нет'
		}).then((result) => {
			if (result.value) {
				swal(
					'Задача выполнена!',
					'Вы молодец!',
					'success'
				);
				task.status = 'finished';
				editTask(task, task._id);
			}
		})
	}

	completeTask = () => {
		const {task} = this.props;
		if (task.stages.length > 0) {
			const isStagesCompleted = task.stages.every(stage => stage.done);
			if (isStagesCompleted) {
        this.confirmComplete();
			} else {
        swal(
          'Стоп!',
          'Сначала выполните все этапы задачи!',
          'error'
        )
			}
		} else {
			this.confirmComplete();
		}
	};
	
	render() {
		const {task, role} = this.props;
		const isFinished = task.status === 'finished';
		const startDate = moment(task.startDate).format('DD.MM.YYYY HH:mm');
		const endDate = moment(task.endDate).format('DD.MM.YYYY HH:mm');
		const isUser = role === 'user';

		if (this.props.visibleModal) {
			return (
				<div onClick={this.props.hideModalHandler} className="task-modal_backdrop">
					<div onClick={this.props.hideModalHandler} className="task-modal">
            {<div className="stages_block">
              {task.stages.map((stage, id) =>
                <span key={id} className={`stage-marker ${stage.done ? 'done' : 'in_progress'}`} />
              )}
            </div>}
						<ConfirmModal
							deleteHandler={this.props.deleteTaskHandler}
							cancelHandler={this.hideConfirmModal}
							visible={this.state.visibleConfirm}/>
						<button className="task-modal_close"><MdClose/></button>
						<div className="task-modal_item task-modal_title">{task.title}</div>
						<div className="task-modal_item task-modal_date">
							{task.priority && <img className="task-modal_fire" src={fire} alt="fire"/>}
							<div className='one-task_header_date'>{translate('oneTaskInfo.startDate')} {startDate}</div>
							<div className='one-task_header_date'>{translate('oneTaskInfo.endDate')} {endDate}</div>
						</div>
						<div className="task-modal_item task-modal_category">
							<b>{translate('mainPage.taskModal.category')}: </b>
							{task.categoryName}
						</div>
						<div className="task-modal_item task-modal_user">
							<b>{translate('mainPage.taskModal.user')}: </b>
							{task.user && task.user}
						</div>
						<div className="task-modal_item task-modal_description">
							<b className="task-modal_label">{translate('mainPage.taskModal.description')}</b>
							<p>{task.description}</p>
						</div>
						<div className="task-modal_editor-block">
							{(task.stages.length > 0) &&
							<button onClick={this.goToCountdown}
							        className="task-modal_button task-modal_button--cd">
								<MdList/>
							</button>}
							{!isFinished && <button onClick={this.completeTask}
							                        className="task-modal_button task-modal_button--complete">
								<MdThumbUp/>
							</button>}
							{!(task.isCreatedByAdmin && isUser) && <button onClick={this.showConfirmModal}
							        className="task-modal_button task-modal_button--delete">
								<MdDelete/>
							</button>}
							{(!isFinished && !(task.isCreatedByAdmin && isUser)) && <button onClick={this.props.editTaskHandler}
							                        className="task-modal_button task-modal_button--edit">
								<MdEdit/>
							</button>}
						</div>
					</div>
				</div>
			)
		} else return null;
	}
	
}

export default TaskModal;
