import React, { Component } from "react";
import "./EditNewsForm.css"
import { connect } from "react-redux";
import { getOneNews, saveNewsChanges } from "../../store/actions/newses";
import Button from "../Button/Button";
import Input from "../Input/Input";
import { FaImage, FaPaperclip } from "react-icons/lib/fa/index";
import Layout from "../Layout/Layout";
import config from "../../config";
import notFound from '../../assets/images/not-found.jpeg';
import TextArea from "../TextArea/TextArea";
import {translate} from "../../localization/i18n";


class EditNewsForm extends Component {
	
	state = {
		title: '',
		description: '',
		image: null,
		newImage: null,
		file: '',
		preview: null,
	};
	
	componentDidMount() {
		this.props.getOneNews(this.props.match.params.id).then(response => {
			this.setState({
				title: response.news.title,
				description: response.news.description,
                image: response.news.image,
				file: response.news.file
			})
		})
		
	};

	submitNewsFormHandler = (event) => {
		event.preventDefault();

		const formData = new FormData();
		const stateData = {...this.state};

        if(this.state.newImage) {
        	stateData.image = this.state.newImage;
        }

		Object.keys(stateData).forEach(key => {
			if (key !== 'preview'  && key !=='newImage') formData.append(key, stateData[key]);
		});

		this.props.saveNewsChanges(this.props.match.params.id, formData);
	};

	inputChangeHandler = event => {
		this.setState({
			[event.target.name]: event.target.value
		});
	};
	
	imageChangeHandler = event => {
		if (event.target.files && event.target.files[0]) {
			let reader = new FileReader();
			reader.onload = (event) => {
				this.setState({preview: event.target.result});
			};
			reader.readAsDataURL(event.target.files[0]);
		}

		this.setState({
			[event.target.name]: event.target.files[0]
		});
	};
	
	fileChangeHandler = event => {
		this.setState({
			[event.target.name]: event.target.files[0]
		});
	};
	
	render() {
		let image = notFound;
		if (this.state.image && !this.state.newImage) {
			image = config.apiUrl + 'uploads/' + this.state.image;
		}else {
			image = this.state.newImage
		}
		let imageText, fileText;
		if (this.state.newImage) {
			imageText = this.state.newImage.name
		}

		if (this.props.file) {
			fileText = this.state.file.name
		}


		return (
			<Layout>
				<div className="container">
					<h1 className="edit-news_title">{translate('editNewsPage.title')}</h1>
					<form onSubmit={this.submitNewsFormHandler}>
						<div className="edit-news_image">
							<img src={this.state.preview || image}
							     alt='preview'/>
						</div>
						<div className="edit-news_file">
						</div>
						<div className="edit-news_row row">
							<Input
								value={this.state.title}
								onChange={this.inputChangeHandler}
								name="title"
								type="text"
                                labelText={translate('editNewsPage.newsTitle')}
							/>
						</div>
						<div className="edit-news_row row">
							<TextArea
								value={this.state.description}
								onChange={this.inputChangeHandler}
								name="description"
								type="text"
                                labelText={translate('editNewsPage.description')}
							/>
						</div>
						<div className="edit-news_select add-file">
							<label className="attachFile"
							       htmlFor="file"><FaPaperclip/>{translate('editNewsPage.fileText')}</label>
							<input id="file"
							       style={{display: 'none'}}
							       name="file"
							       onChange={this.fileChangeHandler}
							       type="file"
							/>
							{fileText}
						</div>
						<div className="edit-news_select add-image">
							<label className="addImage" htmlFor="image"><FaImage/>{translate('editNewsPage.imageText')}</label>
							<input id="image"
							       style={{display: 'none'}}
							       name="newImage"
							       onChange={this.imageChangeHandler}
							       type="file"/>
							{imageText}
						</div>
						<div className="row">
							<Button btnClass="edit-news_btn">{translate('editNewsPage.publish')}</Button>
						</div>
					</form>
				</div>
			</Layout>
		);
	}
}


const mapStateToProps = state => ({
	newses: state.newses.newses.news
});

const mapDispatchToProps = dispatch => ({
	saveNewsChanges: (id, oneNewsData) => dispatch(saveNewsChanges(id, oneNewsData)),
	getOneNews: (id) => dispatch(getOneNews(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(EditNewsForm);