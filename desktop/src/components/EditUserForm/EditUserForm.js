import React, { Component } from "react";
import "./EditUserForm.css";
import { connect } from "react-redux";
import { getOneUser, saveUserChanges } from "../../store/actions/users";
import Button from "../Button/Button";
import Input from "../Input/Input";
import { FaImage} from "react-icons/lib/fa/index";
import Layout from "../Layout/Layout";
import config from "../../config";
import notFound from '../../assets/images/not-found.jpeg';
import TextArea from "../TextArea/TextArea";
import {translate} from "../../localization/i18n";
import Select from "react-select";
import 'react-select/dist/react-select.css';


class EditUserForm extends Component {

    state = {
        username: '',
        password: '',
        description: '',
        role: '',
        image: null,
        newImage: null,
        preview: null
    };

    componentWillMount() {
        this.props.getOneUser(this.props.match.params.id).then(response => {
            this.setState({
                username: response.user.username,
                password: response.user.password,
                description: response.user.description,
                role: response.user.role,
                image: response.user.image
            })
        })

    };

    submitNewsFormHandler = (event) => {
        event.preventDefault();

        const formData = new FormData();
        const stateData = {...this.state};

        if(this.state.newImage) {
            stateData.image = this.state.newImage;
        }

        Object.keys(stateData).forEach(key => {
            if (key !== 'preview'  && key !=='newImage') formData.append(key, stateData[key]);
        });

        this.props.saveUserChanges(this.props.match.params.id, formData);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    imageChangeHandler = event => {
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader();
            reader.onload = (event) => {
                this.setState({preview: event.target.result});
            };
            reader.readAsDataURL(event.target.files[0]);
        }

        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };


    selectChangeHandler = (name, role) => {
        if (role) {
            this.setState({[name]: role.value});
        } else {
            this.setState({[name]: ''});
        }
    };


    render() {
        let image = notFound;
        if (this.state.image && !this.state.newImage) {
            image = config.apiUrl + 'uploads/' + this.state.image;
        }else {
            image = this.state.newImage
        }
        let imageText;
        if (this.state.newImage) {
            imageText = <div>{this.state.newImage.name}</div>
        }


        const role = [
            {
                value:'user',
                label: translate('newUser.userRole')
            },{
                value:'admin',
                label: translate('newUser.adminRole')
            },
        ];

        return (
            <Layout>
                <div className="container">
                    <h1 className="edit-user_title">{translate('userEditor.title')}</h1>
                    <form onSubmit={this.submitNewsFormHandler}>
                        <div className="edit-user_image">
                            <img src={this.state.preview || image}
                                 alt='preview'/>
                        </div>
                        <div className="edit-user_row row">
                            {/*{this.props.userError ? <h2 style={{color:"red"}}>{translate('userEditor.usernameError')}</h2>: null }*/}
                            <Input
                                value={this.state.username}
                                onChange={this.inputChangeHandler}
                                name="username"
                                type="text"
                                labelText={translate('userEditor.username')}
                            />
                        </div>
                        <div className="edit-user_row row">
                            <Input
                                value={this.state.password}
                                onChange={this.inputChangeHandler}
                                name="password"
                                type="text"
                                labelText={translate('userEditor.password')}
                            />
                        </div>
                        <div className="edit-user_row row">
							<TextArea
                                value={this.state.description}
                                onChange={this.inputChangeHandler}
                                name="description"
                                type="text"
                                labelText={translate('userEditor.description')}
                            />
                        </div>
                        <div className="edit-user_row row">
                            <Select
                                name="role"
                                value={this.state.role}
                                onChange={role => this.selectChangeHandler('role', role)}
                                options={role}
                                required={true}
                                default={this.state.role}
                                className="edit-user-select"
                                placeholder={translate('newUser.role')}
                            />
                        </div>
                        <div className="edit-user_select add-image">
                            <label className="addImage" htmlFor="image"><FaImage/>{translate('userEditor.imageText')}</label>
                            <input id="image"
                                   style={{display: 'none'}}
                                   name="newImage"
                                   onChange={this.imageChangeHandler}
                                   type="file"/>
                          {imageText}
                        </div>
                        <div className="row">
                            <Button btnClass="edit-news_btn">{translate('userEditor.button')}</Button>
                        </div>
                    </form>
                </div>
            </Layout>
        );
    }
}


const mapStateToProps = state => ({
    user: state.user.user,
    userError: state.user.editUserError
});

const mapDispatchToProps = dispatch => ({
    saveUserChanges: (id, oneUserData) => dispatch(saveUserChanges(id, oneUserData)),
    getOneUser: (id) => dispatch(getOneUser(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(EditUserForm);