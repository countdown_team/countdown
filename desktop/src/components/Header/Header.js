import React, { Fragment, Component } from 'react';
import './Header.css';
import { connect } from "react-redux";
import { logoutUser, setLanguage } from "../../store/actions/users";
import { NavLink } from "react-router-dom";
import { translate } from '../../localization/i18n';

const logo = require("../../assets/images/logo.svg");
const polygon = require("../../assets/images/polygon.svg");
const user = require("../../assets/images/user.svg");
const rus = require("../../assets/images/rus.png");
const eng = require("../../assets/images/usa.png");

class Header extends Component {
	
	state = {
		showMenu: false
	};
	
	changeMenuVisible = () => {
		this.setState({showMenu: !this.state.showMenu});
	};
	
	render() {
		return (
			<Fragment>
				<header>
					<div className="container">
						<NavLink exact to="/"><img className="logo-image" src={logo} alt="logo"/></NavLink>
						<div className="change_lang_block">
							<button
								className={['button_change_lang', this.props.lang === 'RU_ru' ? 'current' : ''].join(' ')}
								style={{background: `url(${rus}) no-repeat center`, backgroundSize: 'cover'}}
								onClick={() => this.props.setLanguage('RU_ru')}/>
							<button
								className={['button_change_lang', this.props.lang === 'EN_en' ? 'current' : ''].join(' ')}
								style={{background: `url(${eng}) no-repeat center`, backgroundSize: 'cover'}}
								onClick={() => this.props.setLanguage('EN_en')}/>
						</div>
						<div id="open-dropdown" className="user">
							<span className="lk"><img src={user} width="41" height="41" alt="user"/></span>
							<span className="open-dropdown"
							      onClick={this.changeMenuVisible}>
							<img src={polygon} width="13"
							     height="8" alt="polygon"/>
						</span>
							{this.state.showMenu &&
							<div className="dropdown-menu">
								<ul className="bottom">
									<li onClick={this.props.logoutUser}><span>{translate('header.exit')}</span></li>
								</ul>
							</div>
							}
						</div>
					</div>
				</header>
			</Fragment>
		);
	}
}

const mapDispatchToProps = dispatch => ({
	logoutUser: () => dispatch(logoutUser()),
	setLanguage: lang => dispatch(setLanguage(lang))
});

const mapStateToProps = state => ({
	lang: state.user.language.language
});


export default connect(mapStateToProps, mapDispatchToProps)(Header);
