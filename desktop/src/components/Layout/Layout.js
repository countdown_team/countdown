import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import './Layout.css';
import Header from "../Header/Header";
import Sidebar from "../Sidebar/Sidebar";
import Footer from "../Footer/Footer";
import AdminSidebar from "../AdminSidebar/AdminSidebar";
import {FaAlignJustify} from "react-icons/lib/fa/index";

class Layout extends Component {


	state = {
		sideBarShown: false
	};


	changeSideBarStatus = () => {
		this.setState({sideBarShown: !this.state.sideBarShown});
	};

	render() {
		return (
			<Fragment>
				<Header/>
				<div className="container flex-container">
                    <span onClick={this.changeSideBarStatus} className="sideBarButton"><FaAlignJustify/></span>
					<div className={['sidebar_wrapper', !this.state.sideBarShown ? 'hidden' : ''].join(' ')}>
						{this.props.user.role === 'admin' ? <AdminSidebar/> : <Sidebar/>}
					</div>
					<div className="contentIn">
						{this.props.children}
					</div>
				</div>
				<Footer/>
			</Fragment>
		);
	}
}

const mapStateToProps = state => ({
	user: state.user.user
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Layout);
