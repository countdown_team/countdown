import React, { Component } from 'react';
import { translate } from "../../localization/i18n";
import PropTypes from "prop-types";
import Select from "react-select";
import InfiniteCalendar, {
	Calendar,
	withRange,
} from 'react-infinite-calendar';
import 'react-infinite-calendar/styles.css';
import './FilterTasks.css';

const calendarTheme = {
    accentColor: '#5e478b',
    floatingNav: {
        background: 'rgba(56, 87, 138, 0.94)',
        chevron: '#FFA726',
        color: '#FFF',
    },
    headerColor: '#965df4',
    selectionColor: '#965df4',
    textColor: {
        active: '#FFF',
        default: '#333',
    },
    todayColor: '#FFA726',
    weekdayColor: '#965df4',
};

const CalendarWithRange = withRange(Calendar);

export default class FilterTasks extends Component {
	
	static propTypes = {
		categoriesList: PropTypes.array.isRequired,
		usersList: PropTypes.array.isRequired,
		onFilterChange: PropTypes.func.isRequired,
	};
	
	state = {
		categoryName: null,
		user: null,
		priority: false,
		startDay: null,
		endDay: null,
	};
	
	selectRange = async range => {
		await this.setState({endDay: range.end, startDay: range.start});
		this.filter();
	};
	
	selectChangeHandler = async (name, value) => {
		await this.setState({[name]: value});
		this.filter();
	};
	
	checkboxInputChangeHandler = async event => {
		await this.setState({priority: event.target.checked});
		this.filter();
	};
	
	filter = () => {
		const filter = {};
		Object.keys(this.state).forEach(key => {
			if (!!this.state[key]) {
				if (key === 'categoryName' || key === 'user') {
					return filter[key] = this.state[key].label;
				}
				return filter[key] = this.state[key];
			}
		});
		this.props.onFilterChange(filter);
	};
	
	handleResetFilter = async() => {
		await this.setState({
			categoryName: null,
			user: null,
			priority: false,
			startDay: null,
			endDay: null,
		});
		this.filter();
	};
	
	render() {
		const maxDate = new Date(new Date().getFullYear() + 2, new Date().getMonth(), new Date().getDate());
		const locale = {
			blank: 'Select a date...',
			headerFormat: 'DD MMM',
			todayLabel: {
				long: 'Сегодня',
			},
			locale: require('date-fns/locale/ru'),
			weekdays: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
			weekStartsOn: 1,
		};
        // module.exports = {
        //     blank: 'Select a date...',
        //     headerFormat: 'ddd, MMM Do',
        //     todayLabel: {
        //         long: 'Today',
        //     },
        //     weekdays: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        //     weekStartsOn: 0,
        // };

		return (
			<div className="tasks-container-filter">
				<div className="row">
					<InfiniteCalendar
						Component={CalendarWithRange}
						width={360}
						height={300}
						selected={{
							start: this.state.startDay,
							end: this.state.endDay,
						}}
						disabledDays={[]}
						minDate={new Date(2018, 0, 1)}
						min={new Date(2018, 0, 1)}
						max={maxDate}
						locale={locale}
						onSelect={this.selectRange}
						theme={calendarTheme}
					/>
				</div>
				<div className="row">
					<label>{translate('mainPage.categoryLabel')}</label>
					<Select
						name="category"
						value={this.state.categoryName}
						onChange={value => this.selectChangeHandler('categoryName', value)}
						options={this.props.categoriesList}
						placeholder={translate('filterTasks.selectCategory')}
					/>
				</div>
				<div className="row">
					<label>{translate('mainPage.userLabel')}</label>
					<Select
						name="users"
						value={this.state.user}
						onChange={value => this.selectChangeHandler('user', value)}
						options={this.props.usersList}
                        placeholder={translate('filterTasks.selectUser')}
					/>
				</div>
				<div className="row">
					<label>{translate('createTaskPage.priority')}</label>
					<input type="checkbox"
					       checked={this.state.priority}
					       onChange={this.checkboxInputChangeHandler}/>
				
				</div>
				<div className="row">
					<button onClick={this.handleResetFilter} className="btn">{translate('mainPage.reset')}</button>
				</div>
			</div>
		);
	}
}
