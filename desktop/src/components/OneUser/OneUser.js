import React, {Component} from 'react';
import {connect} from 'react-redux';
import './OneUser.css';
import config from "../../config";
import notFound from '../../assets/images/not-found.jpeg';
import Layout from "../../components/Layout/Layout";
import {getOneUser} from "../../store/actions/users";
import {translate} from "../../localization/i18n";

class OneUser extends Component {

    componentDidMount() {
        this.props.getOneUser(this.props.match.params.id);
    };

    render() {
        if (this.props.user) {
            let image = notFound;

            if (this.props.user.image) {
                image = config.apiUrl + 'uploads/' + this.props.user.image;
            }
            return (
                <Layout>
                    <div className="content_text_one_user">
                        <h3 className="one_user_big_title">{translate('oneUser.title')}</h3>
                        <div className="one_user_content">
                            <div className="one_user_image">
                                <img src={image} alt='news_image'/>
                            </div>
                            <div className="one_user_info">
                                <div className="one_user_username">
                                    <h2>{this.props.user.username}</h2>
                                </div>
                                <div className="one_user_description">
                                    <p>{translate('oneUser.description')}</p><p>{this.props.user.description}</p>
                                </div>
                                <div className="one_user_role">
                                   <p>{translate('oneUser.role')}</p><p>{this.props.user.role}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </Layout>
            );
        } else {
            return <Layout>
                <div className="content_text">
                    <h3 className="one-user_big-title">Окно просмотра одного пользователя</h3>
                </div>
            </Layout>
        }


    }
}

const mapStateToProps = state => {
    return {
        user: state.user.oneUser
    }
};

const mapDispatchToProps = dispatch => ({
    getOneUser: (id) => dispatch(getOneUser(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(OneUser);