import React, { Component } from 'react';
import './Task.css';
import PropTypes from "prop-types";
import moment from "moment";
import Countdown from "react-countdown-now";
import {translate} from "../../localization/i18n";

class Task extends Component {
	static propTypes = {
		clickHandler: PropTypes.func.isRequired,
		title: PropTypes.string,
		description: PropTypes.string,
		categoryName: PropTypes.string,
		user: PropTypes.string,
		status: PropTypes.string,
		priority: PropTypes.bool,
		_id: PropTypes.any,
	};
	
	renderCountdown = props => {
		return props.completed ? '' :
			<div>{translate('oneTaskInfo.leftTime')} {props.days} {translate('oneTaskInfo.leftDay')} {props.hours} {translate('oneTaskInfo.leftHours')} {props.minutes} {translate('oneTaskInfo.leftMinutes')}</div>
	};
	
	render() {
		let className = '';
		const isTaskDone = this.props.status === 'finished';
		const isTaskOverdue = this.props.status === 'overdue';
		const isTaskPriority = this.props.priority;
		if (isTaskDone) {
			className = 'done';
		} else if (isTaskOverdue) {
			className = 'overdue';
		} else if (isTaskPriority) {
			className = 'one-task--priority';
		}
		const startDate = moment(this.props.startDate).format('DD.MM.YYYY HH:mm');
		const endDate = moment(this.props.endDate).format('DD.MM.YYYY HH:mm');
		
		return (
			<li key={this.props._id}
			    onClick={this.props.clickHandler}
			    className={`one-task ${className}`}>
				<Countdown date={moment(this.props.endDate).valueOf()} renderer={this.renderCountdown}/>
				<div className="one-task_header">
					<span className='one-task_header_date'>{translate('oneTaskInfo.startDate')}{startDate}</span>
					<span className='one-task_header_date'>{translate('oneTaskInfo.endDate')}{endDate}</span>
					<span className='one-task_header_user'>{translate('oneTaskInfo.userName')}{this.props.user}</span>
					<span className='one-task_header_category'>{translate('oneTaskInfo.categoryName')}{this.props.categoryName}</span>
				</div>
				<div className='one-task_title'>{this.props.title}</div>
				<div className='one-task_description'>{this.props.description.slice(0, 100)}...</div>
			</li>
		);
	}
}

export default Task;
