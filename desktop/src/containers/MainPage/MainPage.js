import React, { Component } from 'react';
import { connect } from 'react-redux';
import Task from "../../components/Task/Task";
import Layout from "../../components/Layout/Layout";
import { translate } from "../../localization/i18n";
import {deleteTaskById, editTaskById, fetchTasks} from "../../store/actions/tasks";
import { fetchUsers } from "../../store/actions/users";
import { fetchCategories } from "../../store/actions/categories";
import './MainPage.css';
import TaskModal from "../../components/TaskModal/TaskModal";
import FilterTasks from "../../components/FilterTasks/FilterTasks";
import moment from "moment";

function isBetweenDates(obj, {startDay, endDay}) {
	if (!!startDay && !!endDay) {
		const taskDateMoment = moment(obj.startDate);
		return taskDateMoment.isAfter(moment(startDay)) && taskDateMoment.isBefore(moment(endDay));
	} else {
		return true;
	}
}

function isTaskCategoryEqual(obj, categoryName) {
	if (!!categoryName) {
		return obj.categoryName === categoryName;
	} else return true;
}

function isTaskPriority(obj, priority) {
	if(!!priority) {
		return obj.priority === priority;
	} else return true;
}

function isTaskUserEqua(obj, user) {
	if(!!user) {
		return obj.user === user;
	} else {
		return true;
	}
}

class MainPage extends Component {
	componentDidMount() {
		this.props.onFetchTasks();
		this.props.fetchUsers();
		this.props.fetchCategories();
	}
	
	state = {
		visibleModal: false,
		task: {},
		filters: {}
	};
	
	renderTask = task => {
		return <Task clickHandler={() => this.showModal(task)} key={task._id} {...task} />
	};
	
	showModal = task => {
		this.setState({visibleModal: true, task});
	};
	
	hideModal = () => {
		this.setState({visibleModal: false, task: {}});
	};
	
	deleteTask = () => {
		if (this.state.task._id) {
			this.props.deleteTaskById(this.state.task._id)
		}
	};
	
	editTask = event => {
		event.stopPropagation();
		if (this.state.task._id) {
			this.props.history.push(`/new-task/${this.state.task._id}`);
		}
	};
	
	handleFiltersChange = (filters) => {
		this.setState({filters});
	};
	
	filterTasks = () => {
		const {filters: {startDay, endDay, categoryName, priority, user}} = this.state;
		return this.props.tasks.filter(task => {
			return isTaskCategoryEqual(task, categoryName) && isBetweenDates(task, {startDay, endDay})
				&& isTaskPriority(task, priority) && isTaskUserEqua(task, user);
		});
	};
	
	render() {
		const categoriesList = this.props.categories ? this.props.categories.map(category => {
			return {value: category._id, label: category.categoryName}
		}) : [];
		
		const usersList = this.props.users ? this.props.users.map(user => {
			return {value: user._id, label: user.username}
		}) : [];
		
		const filteredTasks = this.filterTasks();
		
		return (
			<Layout>
				<h2>{translate('mainPage.tasks')}</h2>
				<TaskModal
					visibleModal={this.state.visibleModal}
					task={this.state.task}
					history={this.props.history}
					hideModalHandler={this.hideModal}
					deleteTaskHandler={this.deleteTask}
					editTaskHandler={this.editTask}
					editTask={this.props.editTask}
					role={this.props.user.role}
				/>
				<div className="tasks-container">
					<ul className="tasks-container_list">
						{filteredTasks.map(this.renderTask)}
					</ul>
					<FilterTasks
						categoriesList={categoriesList}
						usersList={usersList}
						onFilterChange={this.handleFiltersChange}
					/>
				</div>
			</Layout>
		)
	}
}

const mapStateToProps = state => ({
	user: state.user.user,
	tasks: state.tasks.tasks,
	users: state.user.users,
	categories: state.categories.categories,
});

const mapDispatchToProps = dispatch => ({
	onFetchTasks: () => dispatch(fetchTasks()),
	fetchUsers: () => dispatch(fetchUsers()),
	fetchCategories: () => dispatch(fetchCategories()),
	deleteTaskById: id => dispatch(deleteTaskById(id)),
	editTask: (task, id) => dispatch(editTaskById(task, id))
});

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
