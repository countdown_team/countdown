import React, { Component } from 'react';
import { connect } from "react-redux";
import Input from "../../components/Input/Input";
import Select from "react-select";
import TimePicker from 'rc-time-picker';
import 'react-select/dist/react-select.css';
import moment from 'moment';
import { createTask, editTaskById, getTaskById } from "../../store/actions/tasks";
import { fetchCategories } from "../../store/actions/categories";
import Button from "../../components/Button/Button";
import Layout from "../../components/Layout/Layout";
import './NewTask.css';
import { fetchUsers } from "../../store/actions/users";
import { translate } from "../../localization/i18n";
import InfiniteCalendar, {
	Calendar,
	withRange,
} from 'react-infinite-calendar';
import 'react-infinite-calendar/styles.css';
import 'rc-time-picker/assets/index.css';
import TextArea from "../../components/TextArea/TextArea";


const calendarTheme = {
    accentColor: '#5e478b',
    floatingNav: {
        background: 'rgba(56, 87, 138, 0.94)',
        chevron: '#FFA726',
        color: '#FFF',
    },
    headerColor: '#aa7df3',
    selectionColor: '#aa7df3',
    textColor: {
        active: '#FFF',
        default: '#333',
    },
    todayColor: '#FFA726',
    weekdayColor: '#aa7df3',
};

const CalendarWithRange = withRange(Calendar);

class NewTask extends Component {
	state = {
		task: {
			user: '',
			title: '',
			description: '',
			categoryName: '',
			priority: false,
			startDate: moment(),
			endDate: moment(),
		},
	};
	
	componentDidMount() {
		this.props.fetchUsers();
		this.props.fetchCategories();
		if (this.props.match.params.id) {
			this.props.getTaskById(this.props.match.params.id).then(response => {
				const task = {
					user: response.data.user._id,
					title: response.data.title,
					description: response.data.description,
					startDate: moment(response.data.startDate),
					endDate: moment(response.data.endDate),
					categoryName: response.data.categoryName._id,
					priority: response.data.priority,
				};
				this.setState({task});
			});
		}
	}
	
	inputChangeHandler = event => {
		const task = {...this.state.task};
		task[event.target.name] = event.target.value;
		this.setState({task});
	};
	
	selectChangeHandler = (name, value) => {
		const task = {...this.state.task};
		if (value) {
			task[name] = value.value;
			this.setState({task});
		} else {
			task[name] = '';
			this.setState({task});
		}
	};
	
	submitFormHandler = e => {
		e.preventDefault();
		if (this.props.match.params.id) {
			this.props.editTaskById(this.state.task, this.props.match.params.id, e.target.name);
		} else {
			this.props.createTask(this.state.task, e.target.name);
		}
	};
	
	changeFormName = value => {
		this.setState({formName: value})
	};
	
	checkboxInputChangeHandler = () => {
		this.setState({
			task: {
				...this.state.task,
				priority: !this.state.task.priority
			}
		})
	};
	
	handleTimeChange = async (name, value) => {
		const date = moment(this.state.task[name]).format('YYYY-MM-DD');
		const time = moment(value).format('HH:mm');
		this.setState({
			task: {
				...this.state.task,
				[name]: moment(date + ' ' + time),
			}
		});
	};
	
	selectDateRange = async range => {
		const startTime = moment(this.state.task.startDate).format('HH:mm');
		const endTime = moment(this.state.task.endDate).format('HH:mm');
		this.setState({
			task: {
				...this.state.task,
				startDate: moment(moment(range.start).format('YYYY-MM-DD') + ' ' + startTime),
				endDate: moment(moment(range.end).format('YYYY-MM-DD') + ' ' + endTime),
			}
		});
	};
	
	render() {
		const users = this.props.users ? this.props.users.map(user => {
			return {value: user._id, label: user.username}
		}) : [];
		
		const categories = this.props.categories ? this.props.categories.map(category => {
			return {value: category._id, label: category.categoryName}
		}) : [];
		const maxDate = new Date(new Date().getFullYear() + 2, new Date().getMonth(), new Date().getDate());
		const locale = {
			blank: 'Select a date...',
			headerFormat: 'DD MMM',
			todayLabel: {
				long: 'Сегодня',
			},
			locale: require('date-fns/locale/ru'),
			weekdays: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
			weekStartsOn: 1,
		};
		
		return (
			<Layout>
				<h3 className="new-task-title">{translate('createTaskPage.pageTitle')}</h3>
				<div className="create-task-container">
					<div className="create-task-container_left-side">
						<form name={this.state.formName && this.state.formName} onSubmit={this.submitFormHandler}
						      className="new-task-form">
							<Select
								name="user"
								value={this.state.task.user}
								onChange={user => this.selectChangeHandler('user', user)}
								options={users}
								required
								className="add-task-select"
								placeholder={translate('createTaskPage.user')}
							/>
							<Input
								type="text"
								value={this.state.task.title}
								name="title"
								onChange={this.inputChangeHandler}
								placeholder={translate('createTaskPage.title')}
								required={true}
								className="add-task-input"
							/>
							<TextArea
								value={this.state.task.description}
								name="description"
								onChange={this.inputChangeHandler}
								placeholder={translate('createTaskPage.description')}
								required={true}
								className="add-task-input"
							/>
							<Select
								name="categoryName"
								value={this.state.task.categoryName}
								onChange={category => this.selectChangeHandler('categoryName', category)}
								options={categories}
								required={true}
								className="add-task-select"
								placeholder={translate('createTaskPage.category')}
							/>
							<label className="priority-label">
								<b>{translate('createTaskPage.priority')}</b>
								<input className="add-task-checkbox" type="checkbox"
								       checked={this.state.task.priority}
								       value={this.state.task.priority}
								       onChange={this.checkboxInputChangeHandler}/>
							</label>
							<div className="new-task-btns-container">
								<Button btnClass="add-task-btn" type="submit"
								        onClick={() => this.changeFormName('create')}>
									{this.props.match.params.id ?
										translate('createTaskPage.save') :
										translate('createTaskPage.create')}
								</Button>
								<Button btnClass="add-task-btn" type="submit"
								        onClick={() => this.changeFormName('countdown')}>{translate('createTaskPage.countDown')}</Button>
							</div>
						</form>
					</div>
					<div className="create-task-container_right-side">
						<InfiniteCalendar
							Component={CalendarWithRange}
							width={360}
							height={300}
							selected={{
								start: this.state.task.startDate,
								end: this.state.task.endDate,
							}}
							disabledDays={[]}
							minDate={new Date(2018, 0, 1)}
							min={new Date(2018, 0, 1)}
							max={maxDate}
							locale={locale}
							onSelect={this.selectDateRange}
                            theme={calendarTheme}
                        />
						<div className='date-picker'>
							<div>
								<label>{translate('createTaskPage.startTime')}</label>
								<TimePicker
									defaultValue={this.state.task.startDate}
									showSecond={false}
									onChange={value => this.handleTimeChange('startDate', value)}
								/>
							</div>
							<div>
								<label>{translate('createTaskPage.endTime')}</label>
								<TimePicker
									value={this.state.task.endDate}
									showSecond={false}
									onChange={value => this.handleTimeChange('endDate', value)}
								/>
							</div>
						</div>
					</div>
				</div>
			</Layout>
		)
	}
}

const mapStateToProps = state => ({
	users: state.user.users,
	categories: state.categories.categories,
	loading: state.tasks.loading
});

const mapDispatchToProps = dispatch => ({
	fetchUsers: () => dispatch(fetchUsers()),
	fetchCategories: () => dispatch(fetchCategories()),
	createTask: (task, name) => dispatch(createTask(task, name)),
	getTaskById: id => dispatch(getTaskById(id)),
	editTaskById: (task, id, name) => dispatch(editTaskById(task, id, name))
});

export default connect(mapStateToProps, mapDispatchToProps)(NewTask);