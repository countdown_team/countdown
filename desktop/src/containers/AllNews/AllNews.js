import React, { Component } from 'react';
import './AllNews.css';
import { connect } from 'react-redux';
import OneNewsList from "../../components/OneNewsList/OneNewsList";
import { deleteOneNews, fetchNewses } from "../../store/actions/newses";
import Layout from "../../components/Layout/Layout";
import {translate} from "../../localization/i18n";


class AllNews extends Component {
	componentDidMount() {
		this.props.fetchNewses();
	}

	render() {

        return (
			<Layout>
				<div>
					<h3 className="all-news_title">{translate('allNews.title')}</h3>
					<ul className="list_of_news">
                        {this.props.newses.map(news => (
                            <OneNewsList
                                key={news._id}
                                id={news._id}
                                title={news.title}
                                description={news.description}
                                image={news.image}
                                date={news.date}
                                file={news.file}
                                onDelete={this.props.deleteOneNews}
								role={this.props.user.role}
                            />
                        ))}
					</ul>
				</div>
			</Layout>
		
		);
	}
}


const mapStateToProps = state => {
	return {
		newses: state.newses.newses,
		user: state.user.user
	}
};

const mapDispatchToProps = dispatch => {
	return {
		fetchNewses: () => dispatch(fetchNewses()),
		deleteOneNews: (id) => dispatch(deleteOneNews(id))
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(AllNews);