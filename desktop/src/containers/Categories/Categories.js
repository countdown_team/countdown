import React, { Component } from 'react';
import { connect } from 'react-redux';
import './Categories.css';
import Layout from "../../components/Layout/Layout";
import { MdDelete, MdEdit } from "react-icons/lib/md/index";
import scrollToComponent from 'react-scroll-to-component';
import { translate } from '../../localization/i18n';
import {
	addCategory,
	deleteCategory,
	editCategory,
	fetchCategories,
	fetchCategoryById
} from "../../store/actions/categories";

class Categories extends Component {
	
	componentDidMount() {
		this.props.fetchCategories();
	}
	
	state = {
		id: null,
		categoryName: '',
		description: '',
		editView: false
	};
	
	inputChangeHandler = event => {
		this.setState({
			[event.target.name]: event.target.value
		});
	};
	
	submitFormHandler = event => {
		event.preventDefault();
		const {categoryName} = this.state;
		const {description} = this.state;
		if (!this.state.editView) {
			this.props.addCategory({categoryName, description}).then(() => {
				this.setState({categoryName: '', description: ''})
			})
		} else {
			this.props.editCategory(this.state.id, {categoryName, description});
			this.setState({
				categoryName: '',
				description: '',
				id: null,
				editView: false
			});
		}
	};
	
	editCategoryHandler = id => {
		this.props.fetchCategoryById(id).then(() => {
			this.setState({
				categoryName: this.props.currentCategory.categoryName,
				description: this.props.currentCategory.description,
				editView: true,
				id: this.props.currentCategory._id
			});
		});
		
		scrollToComponent(this.form, {offset: -40, align: 'top', duration: 500, ease: 'inOutCirc'});
	};
	
	cancelEditor = () => {
		this.setState({
			categoryName: '',
			description: '',
			id: null,
			editView: false
		})
	};
	
	renderCategory = category => (
		<li
			className="categories_item"
			key={category._id}>
			<div className="categories_item_name">
				{category.categoryName}
			</div>
			<button onClick={() => this.props.deleteCategory(category._id)} className="categories_item_delete">
				<MdDelete/>
			</button>
			<button onClick={() => this.editCategoryHandler(category._id)} className="categories_item_edit"><MdEdit/>
			</button>
		</li>
	);
	
	render() {
		return (
			<Layout>
				<h2>{translate('categoriesPage.pageTitle')}</h2>
				<div ref={(div) => {
					this.form = div;
				}} className="add-category-form">
					<h3>{!this.state.editView ? translate('categoriesPage.createCategory') : translate('categoriesPage.editCategory')}</h3>
					<form onSubmit={this.submitFormHandler}>
						<div className="row">
							<label htmlFor="category_name">{translate('categoriesPage.categoryName')}:</label>
							<input name='categoryName'
							       onChange={this.inputChangeHandler}
							       value={this.state.categoryName}
							       type="text"
							       id="category_name"/>
						</div>
						<div className="row">
							<label
								htmlFor="category_description">{translate('categoriesPage.categoryDescription')}:</label>
							<textarea
								onChange={this.inputChangeHandler}
								value={this.state.description}
								id="category_description"
								name="description"/>
						</div>
						<div className="add-category-form_buttons">
							<button
								className="btn add-category-form_save">{translate('categoriesPage.saveButton')}</button>
							{this.state.editView &&
							<button className="btn add-category-form_cancel"
							        onClick={this.cancelEditor}>{translate('categoriesPage.cancelButton')}</button>}
						</div>
					</form>
				</div>
				<h3>{translate('categoriesPage.listTitle')}</h3>
				<ul className='categories_items'>
					{this.props.categories.map(this.renderCategory)}
				</ul>
			</Layout>
		);
	}
}

const mapStateToProps = state => ({
	categories: state.categories.categories,
	currentCategory: state.categories.currentCategory
});

const mapDispatchToProps = dispatch => ({
	fetchCategories: () => dispatch(fetchCategories()),
	deleteCategory: id => dispatch(deleteCategory(id)),
	addCategory: (category) => dispatch(addCategory(category)),
	fetchCategoryById: id => dispatch(fetchCategoryById(id)),
	editCategory: (id, category) => dispatch(editCategory(id, category))
});

export default connect(mapStateToProps, mapDispatchToProps)(Categories);
