import { translate } from "../../../localization/i18n";

export default (data) => {
	if(data.length === 0) return null;
    let newCount = 0;
	let finishedCount = 0;
	let overdueCount = 0;
	data.forEach(d => {
		if (d.status === 'new') {
			newCount++;
		}
		if (d.status === 'finished') {
			finishedCount++;
		}
		if (d.status === 'overdue') {
			overdueCount++;
		}
	});
	return [
		{
			label: translate('diagram.new'),
			value: newCount,
			index: 0,
			color: 'orange'
		},
		{
			label: translate('diagram.finished'),
			value: finishedCount,
			index: 1,
			color: 'green'
		},
		{
			label: translate('diagram.overdue'),
			value: overdueCount,
			index: 2,
			color: 'red'
		}
	];
};
