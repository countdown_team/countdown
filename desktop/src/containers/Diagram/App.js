import React from 'react';
import Pie3D from '../../components/Pie3D/index';
import createData from './utils/data';
import './style1.css'
import Layout from "../../components/Layout/Layout";
import {fetchTasksDiagram} from "../../store/actions/tasks";
import { connect } from "react-redux";
import Select from "react-select";
import {translate} from "../../localization/i18n";
import moment from "moment";
import 'react-select/dist/react-select.css';
import Button from "../../components/Button/Button";



class App extends React.Component {
	state = {
		data: null,
		config: {
			angle: 60,
			h: 60,
			ir: 40,
			size: 100,
			onSliceClick: d => {
				let moved;
				if (this.state.moved.indexOf(d.index) < 0) {
					moved = this.state.moved.push(d.index);
					this.setState(() => moved);
				} else {
					moved = delete this.state.moved[this.state.moved.indexOf(d.index)];
					this.setState(() => moved);
				}
			}
		},
		moved: [],
        year: moment().get('year'),
        month: moment().get('month')
	};
	
	componentDidMount() {
		this.props.onFetchTasks(this.state.year, this.state.month);
	}
	
	async componentWillReceiveProps() {
        if (await this.props.tasks) {
            this.setState({data: createData(this.props.tasks)});
        }
	}

    selectChangeHandler = (name, value) => {
            this.setState({[name]: value.value});
    };

	btnHandler = () =>{
        this.props.onFetchTasks(this.state.year, this.state.month);
    };
	
	render() {
        const year = [{
                value: moment().get('year')-1,
                label:  moment().get('year')-1
            },{
                value: moment().get('year'),
                label:  moment().get('year')
            }];
        const month = [{
            value: 0,
            label: translate(`diagram.month.0`)
        },{
            value: 1,
            label: translate(`diagram.month.1`)
        },{
            value: 2,
            label: translate(`diagram.month.2`)
        },{
            value: 3,
            label: translate(`diagram.month.3`)
        },{
            value: 4,
            label: translate(`diagram.month.4`)
        },{
            value: 5,
            label: translate(`diagram.month.5`)
        },{
            value: 6,
            label: translate(`diagram.month.6`)
        },{
            value: 7,
            label: translate(`diagram.month.7`)
        },{
            value: 8,
            label: translate(`diagram.month.8`)
        },{
            value: 9,
            label: translate(`diagram.month.9`)
        },{
            value: 10,
            label: translate(`diagram.month.10`)
        },{
            value: 11,
            label: translate(`diagram.month.11`)
        }];
		return (
			<Layout>
                {this.state.data ?   <div className="row">
					<div className="col-lg-6">
						<div className='diagram'>
                            <Pie3D data={this.state.data} config={this.state.config}/>
						</div>
					</div>
				</div>: <h2>{translate('diagram.noInfo')}</h2>}
                <div className="diagram-color-info">
                    <div className="diagram-color first">
                        <div className="diagram-first diagram-info"/>
                        <div className="diagram-new diagram-lines"><span>{translate('diagram.new')}</span></div>
                    </div>
                    <div className="diagram-color second">
                        <div className="diagram-second diagram-info"/>
                        <div className="diagram-finished diagram-lines"><span>{translate('diagram.finished')}</span></div>
                    </div>
                    <div className="diagram-color third">
                        <div className="diagram-third diagram-info"/>
                        <div className="diagram-overdue diagram-lines"><span>{translate('diagram.overdue')}</span></div>
                    </div>
                </div>
                <div className="select-container">
                   <Select
                    name="year"
                    value={this.state.year}
                    onChange={year => this.selectChangeHandler('year', year)}
                    options={year}
                    required={true}
                    className="diagram-select"
                    placeholder={this.state.year}
                />
				   <Select
                    name="month"
                    value={this.state.month}
                    onChange={month => this.selectChangeHandler('month', month)}
                    options={month}
                    required={true}
                    className="diagram-select"
                    placeholder={translate(`diagram.month.${this.state.month}`)}
                />

               </div>
                <div className='diagram-btn-container'><Button btnClass="diagram-btn'" type="submit"
                             onClick={this.btnHandler}>{translate('diagram.btn')}</Button></div>
			</Layout>
		);
	}
}

const mapStateToProps = state => ({
	user: state.user.user,
	tasks: state.tasks.diagramTasks,
});

const mapDispatchToProps = dispatch => ({
	onFetchTasks: (year, month) => dispatch(fetchTasksDiagram(year, month))
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
