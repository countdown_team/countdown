import React, { Component } from 'react';
import './AllUsers.css';
import { connect } from 'react-redux';
import OneUserList from "../../components/OneUserList/OneUserList";
import { fetchUsers } from "../../store/actions/users";
import Layout from "../../components/Layout/Layout";
import {deleteOneUser} from "../../store/actions/users";
import {translate} from "../../localization/i18n";


class AllUsers extends Component {
    componentDidMount() {
        this.props.fetchUsers();
    }

    render() {
        return (
            <Layout>
                <div>
                    <h3 className="all-users_title">{translate('allUsers.pageTitle')}</h3>
                    <ul className="list_of_users">
                        {this.props.users.map(user => (
                            <OneUserList
                                key={user._id}
                                id={user._id}
                                username={user.username}
                                description={user.description}
                                image={user.image}
                                onDelete={this.props.deleteOneUser}
                            />
                        ))}
                    </ul>
                </div>
            </Layout>

        );
    }
}


const mapStateToProps = state => {
    return {
        users: state.user.users
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchUsers: () => dispatch(fetchUsers()),
        deleteOneUser: (id) => dispatch(deleteOneUser(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AllUsers);