import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import Layout from "../../components/Layout/Layout";
import {MdAddCircle, MdDelete, MdSave, MdDone, MdCreate} from 'react-icons/lib/md';
import Input from "../../components/Input/Input";
import InputMoment from 'input-moment';
import moment from 'moment';
import './Countdown.css';
import {getTaskById, saveCountdown} from "../../store/actions/tasks";
import Button from "../../components/Button/Button";
import swal from 'sweetalert2';
import {translate} from "../../localization/i18n";

class Countdown extends Component {
  state = {
    m: moment(),
    stages: [],
    show: false,
    id: null,
    task: {}
  };

  inputChangeHandler = (e, id) => {
    let stages = [...this.state.stages];
    stages[id][e.target.name] = e.target.value;
    this.setState({stages});
  };

  componentDidMount() {
    this.props.getTaskById(this.props.match.params.id).then(response => {
      let stages = response.data.stages || [];
      this.setState({stages, task: response.data});
    })
  }

  componentWillUnmount() {
    this.setState({stages: []});
  }

  addStage = () => {
    let stages = [...this.state.stages];
    let stage = {
      title: '',
      description: '',
      date: '',
      time: '',
      done: false,
      isEditable: true
    };
    stages.push(stage);
    this.setState({stages: stages});
  };

  handleChange = m => {
    this.setState({m});
  };

  removeStage = key => {
    const swalWithBootstrapButtons = swal.mixin({
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: true
    });

    swalWithBootstrapButtons({
      title: translate('countdownPage.remove.title'),
      text: translate('countdownPage.remove.text'),
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: translate('countdownPage.remove.confirm'),
      cancelButtonText: translate('countdownPage.remove.cancel'),
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        swalWithBootstrapButtons(
          translate('countdownPage.remove.deleted'),
          translate('countdownPage.remove.fileDeleted'),
          'success'
        );
        let stages = [...this.state.stages];
        stages.splice(key, 1);
        this.setState({stages});
      } else if (result.dismiss === swal.DismissReason.cancel) {
        swalWithBootstrapButtons(
          translate('countdownPage.remove.canceled'),
          translate('countdownPage.remove.fileCanceled'),
          'error'
        )
      }
    });
  };

  datePickerShow = key => {
    this.setState({show: !this.state.show, id: key});
  };

  handleSave = key => {
    let stages = [...this.state.stages];
    stages[key].date = this.state.m.format('DD/MM/YYYY');
    stages[key].time = this.state.m.format('HH:mm');
    this.setState({stages, show: false});
  };

  completeStage = key => {
    let stages = [...this.state.stages];
    stages[key].done = true;
    this.setState({stages});
  };

  save = () => {
    const task = this.state.task;
    task.stages = this.state.stages;
    this.props.saveCountdown(task);
  };

  edit = (e, id) => {
    e.preventDefault();
    let stages = [...this.state.stages];
    stages[id].isEditable = !stages[id].isEditable;
    this.setState({stages});
  };

  confirmComplete = key => {
    const swalWithBootstrapButtons = swal.mixin({
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: true
    });

    swalWithBootstrapButtons({
      title: translate('countdownPage.complete.title'),
      text: translate('countdownPage.complete.text'),
      type: 'question',
      showCancelButton: true,
      confirmButtonText: translate('countdownPage.complete.confirm'),
      cancelButtonText: translate('countdownPage.complete.cancel'),
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        swalWithBootstrapButtons(
          translate('countdownPage.complete.done'),
          translate('countdownPage.complete.goodJob'),
          'success'
        );
        this.completeStage(key);
      } else if (result.dismiss === swal.DismissReason.cancel) {
        swalWithBootstrapButtons(
          translate('countdownPage.complete.notDone'),
          translate('countdownPage.complete.hurryUp'),
          'info'
        )
      }
    });
  };

  handleFormSubmit = (key, e) => {
    e.preventDefault();
    this.edit(e, key);
  };

  render() {
    const task = this.state.task;
    const isDone = task.status === 'finished';
    const isUser = this.props.user.role === 'user';
    return (
      <Fragment>
        <Layout>
          <h1 className="new-task-title">{task.title && task.title}</h1>
          <h2 className="stages-title">{translate('countdownPage.stages')}</h2>
          <div className="countdown-container">
            <div className="datepicker-container" style={{display: this.state.show ? 'block' : 'none'}}
                 onClick={() => this.setState({show: false})}>
              <div onClick={e => e.stopPropagation()}>
                <div>
                  <InputMoment
                    moment={this.state.m}
                    onChange={this.handleChange}
                    minStep={1}
                    hourStep={1}
                    onSave={() => this.handleSave(this.state.id)}
                    prevMonthIcon="ion-ios-arrow-left"
                    nextMonthIcon="ion-ios-arrow-right"
                  />
                </div>
              </div>
            </div>
            {this.state.stages.map((stage, key) =>
              <form className="stage-container" onSubmit={this.handleFormSubmit.bind(null, key)} key={key}>
                <div className="stage-container-body">
                  {!stage.isEditable && <h3 className="stage-title">{key + 1}. {stage.title}</h3>}
                  {!stage.isEditable && <div className="stage-datetime">{`${stage.date} ${stage.time}`}</div>}
                  {stage.isEditable ?
                    <Fragment>
                      <Input
                        type="text"
                        value={stage.title}
                        name="title"
                        onChange={(e) => this.inputChangeHandler(e, key)}
                        required={true}
                        className="countdown-input"
                        placeholder={translate('countdownPage.title')}
                        labelText={translate('countdownPage.title')}
                      />
                      <div className="row">
                        <label
                          htmlFor={`countdown_description-${key}`}>{translate('countdownPage.description')}</label>
                        <textarea
                          onChange={(e) => this.inputChangeHandler(e, key)}
                          value={stage.description}
                          name="description"
                          id={`countdown_description-${key}`}
                          rows={7}
                        />
                      </div>
                      <Input
                        type="text"
                        value={stage.date ? `${stage.date} ${stage.time}` : ''}
                        onClick={() => this.datePickerShow(key)}
                        required={true}
                        placeholder={translate('countdownPage.date')}
                        className="countdown-input"
                        name="date"
                        readOnly={true}
                        labelText={translate('countdownPage.date')}
                      />
                    </Fragment> : <p className="description">{stage.description}</p>
                  }
                </div>
                <div className="stage-container-footer">
                  {!stage.done ? <div className="else-container" style={{justifyContent: isUser ? 'flex-end' : 'space-around'}}>
                      {!(isUser && task.isCreatedByAdmin) && <button className="stage-footer-icons save" type="submit">{stage.isEditable ?
                        <MdSave className="footer-icons"/> :
                        <MdCreate className="footer-icons"/>}</button>}
                      {stage.isEditable &&
                      <button className="stage-footer-icons delete"
                           onClick={() => this.removeStage(key)}><MdDelete
                        className="footer-icons"/></button>}
                      {!stage.isEditable &&
                      <div className="stage-footer-icons done"
                           onClick={() => this.confirmComplete(key)}><MdDone
                        className="footer-icons"/></div>}
                    </div>
                    : <div className="task-completed">{translate('countdownPage.completed')}</div>}
                </div>
              </form>
            )}
            {(!isDone && !(isUser && task.isCreatedByAdmin)) && <div className="plus-icon-container" onClick={this.addStage}><MdAddCircle
              className="plus-icon"/></div>}
          </div>
          {!isDone && <Button onClick={this.save} btnClass="countdown-btn">{translate('countdownPage.button')}</Button>}
        </Layout>
      </Fragment>
    )
  }
}

const mapStateToProps = state => ({
  task: state.tasks.task,
  user: state.user.user
});

const mapDispatchToProps = dispatch => ({
  saveCountdown: data => dispatch(saveCountdown(data)),
  getTaskById: id => dispatch(getTaskById(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Countdown);