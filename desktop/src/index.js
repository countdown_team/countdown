import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import { PersistGate } from 'redux-persist/integration/react';
import registerServiceWorker from './registerServiceWorker';
import configureStore, { history } from './store/configureStore';
import { NotificationManager } from 'react-notifications';
import axios from './axios-api';
import App from './App';
import './index.css';
import {translate} from "./localization/i18n";

const {persistor, store} = configureStore();

axios.interceptors.request.use(config => {
	try {
		config.headers['Token'] = store.getState().user.token;
	} catch (e) {
		// do nothing
	}
	return config;
});

axios.interceptors.response.use(function (response) {
  return response;
}, function (error) {
  if (401 === error.response.status) {
    history.push('/login');
    return NotificationManager.error('Error', translate('session.text'));
  }
});

const app = (
	<Provider store={store}>
		<PersistGate loading={null} persistor={persistor}>
			<ConnectedRouter history={history}>
				<App/>
			</ConnectedRouter>
		</PersistGate>
	</Provider>
);

ReactDOM.render(app, document.getElementById('root'));
registerServiceWorker();
