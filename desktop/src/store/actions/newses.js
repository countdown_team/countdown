import axios from '../../axios-api';
import { ADD_ONE_NEWS_SUCCESS, FETCH_NEWSES_SUCCESS, FETCH_ONE_NEWS_SUCCESS } from "./actionType";
import { push } from "react-router-redux";

const fetchNewsesSuccess = newses => {
	return {type: FETCH_NEWSES_SUCCESS, newses};
};

export const addOneNewsSuccess = () => {
	return {type: ADD_ONE_NEWS_SUCCESS};
};

export const addNews = (newsData) => {
	return dispatch => {
		axios.post('/newses', newsData).then(
			response => {
			 dispatch(addOneNewsSuccess());
			 dispatch(push('/newses'));
            }
		)
	}
};

export const fetchNewses = () => {
	return dispatch => {
		return axios.get('/newses').then(
			response => dispatch(fetchNewsesSuccess(response.data))
		)
	};
};

export const fetchOneNewsSuccess = (news) => {
	return {type: FETCH_ONE_NEWS_SUCCESS, news}
};

export const getOneNews = (id) => {
	return dispatch => {
		return axios.get('/newses/' + id).then(
			response => {
				return dispatch(fetchOneNewsSuccess(response.data))
			}
		)
	}
};

export const deleteOneNews = (id) => {
	return dispatch => {
		return axios.delete('/newses/' + id).then(
			response => {
				return dispatch(fetchNewses())
			}
		)
	}
};

export const saveNewsChanges = (id, oneNewsData) => {
    return dispatch => {
		return axios.put(`/newses/` + id, oneNewsData).then(
			response => {
				return dispatch(push('/newses'))
			}
		)
	}
};
