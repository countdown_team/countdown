import axios from "../../axios-api";
import {
	ADD_CATEGORY_FAILURE, FETCH_CATEGORIES_BY_ID_FAILURE, FETCH_CATEGORIES_BY_ID_SUCCESS,
	FETCH_CATEGORIES_FAILURE,
	FETCH_CATEGORIES_SUCCESS
} from "./actionType";

const addCategoryFailure = (error) => {
	return {type: ADD_CATEGORY_FAILURE, error};
};

export const addCategory = (category) => {
	return dispatch => {
		return axios.post('/categories', category).then(
			response => dispatch(fetchCategories()),
			error => dispatch(addCategoryFailure(error))
		);
	}
};

const fetchCategoriesSuccess = categories => {
	return {type: FETCH_CATEGORIES_SUCCESS, categories};
};

const fetchCategoriesFailure = error => {
	return {type: FETCH_CATEGORIES_FAILURE, error};
};

export const fetchCategories = () => {
	return dispatch => {
		axios.get('/categories').then(response => {
			dispatch(fetchCategoriesSuccess(response.data));
		}, error => {
			dispatch(fetchCategoriesFailure(error));
		});
	}
};

export const deleteCategory = id => {
	return dispatch => {
		axios.delete(`/categories/${id}`).then(
			() => dispatch(fetchCategories())
		);
	}
};

export const fetchCategoryById = id => {
	return dispatch => {
		return axios.get(`/categories/${id}`).then(
			response => dispatch(fetchCategoryByIdSuccess(response.data),
				error => dispatch(fetchCategoryByIdFailure(error)))
		)
	}
};

const fetchCategoryByIdSuccess = category => {
	return {type: FETCH_CATEGORIES_BY_ID_SUCCESS, category};
};
const fetchCategoryByIdFailure = category => {

	return {type: FETCH_CATEGORIES_BY_ID_FAILURE, category};
};

export const editCategory = (id, category) => {
	return dispatch => {
		return axios.put(`/categories/${id}`, category).then(
			response => dispatch(fetchCategories()),
			error => {})
	}
};
