import axios from '../../axios-api';
import {push} from 'react-router-redux';
import i18next from 'i18next';
import {NotificationManager} from 'react-notifications';
import {
    CHANGE_LANGUAGE,
    FETCH_ONE_USER_SUCCESS,
    FETCH_USERS_FAILURE,
    FETCH_USERS_SUCCESS,
    LOGIN_USER_FAILURE,
    LOGIN_USER_SUCCESS, LOGOUT_USER,
    REGISTER_USER_FAILURE,
    REGISTER_USER_SUCCESS, USER_EDIT_FAILURE,
} from "./actionType";

const registerUserSuccess = () => {
    return {type: REGISTER_USER_SUCCESS};
};

const registerUserFailure = error => {
    return {type: REGISTER_USER_FAILURE, error};
};

export const registerUser = userData => {
    return dispatch => {
        axios.post('/users', userData).then(
            response => {
                dispatch(registerUserSuccess());
                dispatch(push('/users'));
                NotificationManager.success('Success', 'Registration successful');
            },
            error => {
                dispatch(registerUserFailure(error.response));
            }
        )
    }
};

const loginUserSuccess = (user, token) => {
    return {type: LOGIN_USER_SUCCESS, user, token};
};

const loginUserFailure = error => {
    return {type: LOGIN_USER_FAILURE, error};
};
const UserEditFailure = error => {
    return {type: USER_EDIT_FAILURE, error};
};

export const loginUser = userData => {
    return dispatch => {
        return axios.post('/users/sessions', userData).then(
            response => {
                dispatch(loginUserSuccess(response.data.user, response.data.token));
                dispatch(push('/'));
                NotificationManager.success('Success', response.data.message);
            },
            error => {
                const errorObj = error.response ? error.response.data : {error: 'No internet'};
                dispatch(loginUserFailure(errorObj));
            }
        )
    }
};

export const logoutUser = () => {
    return (dispatch) => {
        axios.delete('/users/sessions').then(
            response => {
                dispatch({type: LOGOUT_USER});
                dispatch(push('/'));
                NotificationManager.success('Success', 'Logout successful');
            }, error => {
                NotificationManager.error('Error', 'Could not logout');
            }
        );
    }
};


const fetchUsersSuccess = users => {
    return {type: FETCH_USERS_SUCCESS, users};
};

const fetchUsersFailure = error => {
    return {type: FETCH_USERS_FAILURE, error};
};

export const fetchUsers = () => {
    return dispatch => {
        axios.get('/users').then(response => {
            dispatch(fetchUsersSuccess(response.data));
        }, error => {
            dispatch(fetchUsersFailure(error));
        })
    };
};
export const fetchOneUserSuccess = (user) => {
    return {type: FETCH_ONE_USER_SUCCESS, user}
};


export const changeLanguage = (langFunction) => {
    return dispatch => {
        dispatch({
            type: CHANGE_LANGUAGE,
            language: langFunction
        });
    }
};

export const setLanguage = (language) => {
    return dispatch => {
        i18next.init({
            lng: language,
            resources: require(`../../localization/${language}.js`)
        });
        dispatch(changeLanguage(i18next));
    }
};

export const getOneUser = (id) => {
    return dispatch => {
        return axios.get('/users/' + id).then(
            response => {
                return dispatch(fetchOneUserSuccess(response.data))
            }
        )
    }
};

export const deleteOneUser = (id) => {
    return dispatch => {
        return axios.delete('/users/' + id).then(
            response => {
                return dispatch(fetchUsers())
            }
        )
    }
};

export const saveUserChanges = (id, oneUserData) => {
    return dispatch => {
        return axios.put(`users/${id}`, oneUserData).then(
            response => {
                return dispatch(push('/users'))
            },
            error => {
                const err = error.response ? error.response.data : {error: 'No internet'};
                dispatch(UserEditFailure(err));
            }
    )
    }
};
