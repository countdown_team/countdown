import {
	ADD_CATEGORY_FAILURE,
	FETCH_CATEGORIES_BY_ID_SUCCESS,
	FETCH_CATEGORIES_FAILURE,
	FETCH_CATEGORIES_SUCCESS
} from "../actions/actionType";
import merge from 'xtend';

const initialState = {
	categories: [],
	error: null,
	currentCategory: {}
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_CATEGORIES_SUCCESS:
			return merge(state, {categories: action.categories});
		case FETCH_CATEGORIES_FAILURE:
			return {...state, error: action.error};
		case ADD_CATEGORY_FAILURE:
			return merge(state, {error: action.error});
		case FETCH_CATEGORIES_BY_ID_SUCCESS:
			return merge(state, {currentCategory: action.category});
		default:
			return state;
		
	}
};

export default reducer;