import { FETCH_NEWSES_SUCCESS, FETCH_ONE_NEWS_SUCCESS } from "../actions/actionType";

const initialState = {
	newses: [],
	news: null,
	loading: false
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_NEWSES_SUCCESS:
			return  {...state, newses: action.newses};
		case FETCH_ONE_NEWS_SUCCESS:
			return {...state, news: action.news};
		default:
			return state;
		
	}
};

export default reducer;