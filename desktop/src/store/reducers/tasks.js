import {
    START_REQUEST,
    CREATE_TASK_FAILURE,
    CREATE_TASK_SUCCESS,
    FETCH_TASKS_SUCCESS, FETCH_TASK_INFO_SUCCESS, FETCH_TASKS_DIAGRAM_SUCCESS
} from "../actions/actionType";
import merge from 'xtend';

const initialState = {
	error: null,
	loading: false,
	tasks: [],
	task: {},
	diagramTasks: null
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case START_REQUEST:
			return {...state, loading: true};
		case CREATE_TASK_SUCCESS:
			return {...state, loading: false, task: action.task};
		case CREATE_TASK_FAILURE:
			return {...state, loading: false};
        case FETCH_TASKS_DIAGRAM_SUCCESS:
            return {...state, loading: false, diagramTasks: action.tasks};
		case FETCH_TASKS_SUCCESS:
			const tasks = action.tasks.map(task => ({
				...task,
				categoryName: task.categoryName.categoryName,
				user: task.user.username
			}));
			return merge(state, {tasks: tasks});
 case FETCH_TASK_INFO_SUCCESS:
      return merge(state, {task: action.task});
		default:
			return state;
	}
};

export default reducer;