export const EN_en = {
	translation: {
		loginPage: {
			login: 'Username',
			password: 'Password',
			enter: 'Log in'
		},
		header: {
			home: 'Home',
			about: 'About',
			calendar: 'Calendar',
			faq: 'FAQ',
			exit: 'Log out'
		},
		mainPage: {
			tasks: 'Tasks',
			categoryLabel: 'Select category',
			userLabel: 'Select user',
			reset: "Reset",
			taskModal: {
				category: "Category",
				user: "Executor",
				date: "Date",
				description: "Description",
				confirmDelete: "Are you sure you want to delete the task and lose all the data about it?",
				cancel: "Cancel",
				delete: "Delete",
			},
		},
		newUser: {
			title: 'Admin panel',
			subtitle: 'Add new user',
			username: 'Username',
			password: 'Password',
			description: 'Description',
			role: 'Role',
			register: 'Register',
			userRole: 'User',
			adminRole: 'Administrator',
			imageText: 'Attach an image'
		},
		sidebar: {
			admin: 'Administrator',
			tasks: 'Tasks',
			addUser: 'Add new user',
			addNews: 'Create your news',
			addTask: 'Create task',
			user: 'User',
			allUsers: 'All users',
			news: 'News',
			categories: 'Categories',
			diagram: 'Diagram'
		},
		addNewsPage: {
			title: 'Create news',
			newsTitle: 'Title',
			newsDescription: 'Description',
			publish: 'Publish',
			imageText: 'Attach an image',
			fileText: 'Attach a file'
		},
		editNewsPage: {
			title: 'Insert your changes',
			newsTitle: 'Title',
			description: 'Description',
			publish: 'SAVE CHANGES',
			imageText: 'Attach another image',
			fileText: 'Attach another file'
		},
		createTaskPage: {
			pageTitle: 'Create task',
			title: 'Title',
			description: 'Description',
			date: 'Select a date',
			priority: 'Priority',
			create: 'Create',
			save: 'Save',
			countDown: 'Countdown',
			user: 'Choose user',
			category: 'Choose category',
			startTime: 'Time of the beginning',
			endTime: 'Deadline'
		},
		categoriesPage: {
			pageTitle: 'Categories',
			createCategory: 'Create category',
			editCategory: 'Edit category',
			categoryName: 'Category name',
			categoryDescription: 'Category description',
			saveButton: 'Save',
			cancelButton: 'Cancel',
			listTitle: 'List of categories'
		},
		allNews: {
			title: 'All news'
		},
		oneNewsList: {
			linkTitle: 'Download file'
		},
		allUsers: {
			pageTitle: 'All users'
		},
		oneUserList: {
			shortDescription: 'Short description: '
		},
		oneUser: {
			title: 'One user window',
			description: 'Description: ',
			role: 'Role: '
		},
		userEditor: {
			title: 'Insert your changes',
			username: 'Username',
			password: 'Password',
			description: 'Description',
			role: 'Role',
			button: 'SAVE CHANGES',
			imageText: 'Attach another image',
			usernameError: 'Username already exists'
		},
		diagram: {
			new: 'New tasks',
			finished: 'Finished tasks',
			overdue: 'Overdue tasks',
			month:{
				0: 'January',
				1: 'February',
				2: 'March',
				3: 'April',
				4: 'May',
				5: 'June',
				6: 'July',
				7: 'August',
				8: 'September',
				9: 'October',
				10: 'November',
				11: 'December'
			},
			btn: 'Show',
			noInfo: 'Information not present'
		},
		countdownPage: {
			completed: 'Task completed',
			button: 'Save',
			stages: 'Stages',
			title: 'Title',
			description: 'Description',
			date: 'Choose a date',
			remove: {
				title: 'Are you sure?',
				text: "You won't be able to revert this!",
				confirm: 'Yes, delete it!',
				cancel: 'No, cancel!',
				deleted: 'Deleted!',
				fileDeleted: 'Your file has been deleted.',
				canceled: 'Canceled',
				fileCanceled: 'Your imaginary file is safe :)'
			},
			complete: {
				title: 'Are you sure that you completed the task?',
				text: "You will not be able to cancel!",
				confirm: 'Yes!',
				cancel: 'No',
				done: 'Completed',
				goodJob: 'Good job!',
				notDone: 'Canceled',
				hurryUp: 'Hurry up :)'
			}
		},
        oneTaskInfo: {
            startDate: 'Date of the beginning: ',
            endDate: 'Deadline: ',
            userName: 'Executor: ',
            categoryName: 'Category: ',
			leftTime: 'Left time: ',
			leftDay: 'd.',
			leftHours: 'h.',
			leftMinutes: 'm.',
			leftSeconds: 's'
        },
		filterTasks: {
			selectCategory: 'Category',
			selectUser: 'User'
		},
		session: {
			text: 'Your session has expired!',
			error: 'Error!'
		}
	}
};