import i18next from 'i18next';
export const translate = (msg = '', options) => (i18next.t(msg, options));
