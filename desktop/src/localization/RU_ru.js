export const RU_ru = {
  translation: {
    loginPage: {
      login: 'Логин',
      password: 'Пароль',
      enter: 'Войти'
    },
    header: {
      home: 'Главная',
      about: 'О программе',
      calendar: 'Календарь',
      faq: 'Вопросы-ответы',
      promo: 'Промокод',
      profile: 'Мой профиль',
      exit: 'Выйти'
    },
    mainPage: {
      tasks: 'Задачи',
      categoryLabel: 'Выберите категорию',
      userLabel: 'Выберите пользователя',
      reset: "Сброс",
      taskModal: {
        category: "Категория",
        user: "Исполнитель",
        date: "Дата",
        description: "Описание",
        confirmDelete: "Вы уверены что хотите удалить таск и потерять все данные о нем?",
        cancel: "Отмена",
        delete: "Удалить",
      },
    },
    newUser: {
      title: 'Админ панель',
      subtitle: 'Добавить нового пользователя',
      username: 'Логин',
      password: 'Пароль',
      description: 'Описание',
      role: 'Роль',
      register: 'Зарегистрировать',
      userRole: 'Пользователь',
      adminRole: 'Администратор',
      imageText: 'Прикрепить фото'
    },
    sidebar: {
      admin: 'Администратор',
      tasks: 'Задачи',
      news: 'Новости',
      categories: 'Категории',
      addUser: 'Добавить пользователя',
      addNews: 'Создать новость',
      addTask: 'Создать задачу',
      user: 'Пользователь',
      allUsers: 'Все пользователи',
      diagram: 'Диаграмма'
    },
    addNewsPage: {
      title: 'Создать новость',
      newsTitle: 'Заголовок',
      newsDescription: 'Описание',
      publish: 'Опубликовать',
      imageText: 'Прикрепить фото',
      fileText: 'Прикрепить файл'
    },
    editNewsPage: {
      title: 'Внести желаемые изменения',
      newsTitle: 'Заголовок',
      description: 'Описание',
      publish: 'СОХРАНИТЬ ИЗМЕНЕНИЯ',
      imageText: 'Прикрепить другое фото',
      fileText: 'Прикрепить другой файл'
    },
    createTaskPage: {
      pageTitle: 'Создать задачу',
      title: 'Заголовок',
      description: 'Описание',
      date: 'Выберите дату',
      priority: 'Приоритет',
      create: 'Создать',
      save: 'Сохранить',
      countDown: 'Обратный отсчет',
      user: 'Выберите пользователя',
      category: 'Выберите категорию',
      startTime: 'Время начала',
      endTime: 'Время конца'
    },
    categoriesPage: {
      pageTitle: 'Категории',
      createCategory: 'Создать категорию',
      editCategory: 'Редактировать категорию',
      categoryName: 'Название категории',
      categoryDescription: 'Описание категории',
      saveButton: 'Сохранить',
      cancelButton: 'Отменить',
      listTitle: 'Список категории'
    },
    allNews: {
      title: 'Все новости'
    },
    oneNewsList: {
      linkTitle: 'Скачать файл'
    },
    allUsers: {
      pageTitle: 'Все пользователи'
    },
    oneUserList: {
      shortDescription: 'Краткое описание: '
    },
    oneUser: {
      title: 'Окно просмотра пользователя',
      description: 'Описание: ',
      role: 'Роль: '
    },
    userEditor: {
      title: 'Внести желаемые изменения',
      username: 'Имя пользователя',
      password: 'Пароль',
      description: 'Описание',
      role: 'Роль',
      button: 'СОХРАНИТЬ ИЗМЕНЕНИЯ',
      imageText: 'Прикрепить другое фото',
      usernameError: 'Такой пользователь есть'
    },
    diagram: {
      new: 'Новые задачи',
      finished: 'Выполненные задачи',
      overdue: 'Просроченные задачи',
      month: {
        0: 'Январь',
        1: 'Февраль',
        2: 'Март',
        3: 'Апрель',
        4: 'Май',
        5: 'Июнь',
        6: 'Июль',
        7: 'Август',
        8: 'Сентябрь',
        9: 'Октябрь',
        10: 'Ноябрь',
        11: 'Декабрь'
      },
      btn: 'Показать',
      noInfo: 'Информация отсутствует'
    },
    countdownPage: {
      completed: 'Задача выполнена',
      button: 'Сохранить',
      stages: 'Этапы',
      title: 'Заголовок',
      description: 'Описание',
      date: 'Выберите дату',
      remove: {
        title: 'Вы уверены?',
        text: "Вы не сможете вернуть обратно!",
        confirm: 'Да, удалить!',
        cancel: 'Нет, отменить!',
        deleted: 'Удалено!',
        fileDeleted: 'Этап был удален.',
        canceled: 'Отменено',
        fileCanceled: 'Отменено удаление этапа'
      },
      complete: {
        title: 'Вы уверены что выполнили задание?',
        text: "Отменить будет нельзя!",
        confirm: 'Да!',
        cancel: 'Нет',
        done: 'Выполнено',
        goodJob: 'Вы молодец!',
        notDone: 'Отменено',
        hurryUp: 'Скорее доделывайте :)'
      }
    },
    oneTaskInfo: {
      startDate: 'Дата начала: ',
      endDate: 'Дата конца: ',
      userName: 'Исполнитель: ',
      categoryName: 'Категория: ',
      leftTime: 'Осталось: ',
      leftDay: 'д.',
      leftHours: 'ч.',
      leftMinutes: 'м.',
      leftSeconds: 'с'
    },
    filterTasks: {
      selectCategory: 'Категория',
      selectUser: 'Пользователь'
    },
    session: {
      text: 'Время сеанса истекло!',
      error: 'Ошибка!'
    }
  }
};