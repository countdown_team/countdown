const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const StagesSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  date: {
    type: String,
    required: true
  },
  time: {
    type: String,
    required: true
  },
  done: Boolean
});

const TasksSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  startDate: {
    type: String,
    required: true
  },
  endDate: {
    type: String,
    required: true
  },
  categoryName: {
    type: Schema.Types.ObjectId,
    ref: 'Categories',
    required: true
  },
  priority: {
    type: Boolean,
    required: true
  },
  user: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'Users'
  },
  stages: {
    type: [StagesSchema]
  },
  status:{
      type: String,
      required: true,
      default: 'new',
      enum: ['new', 'finished', 'overdue']
  },
  isCreatedByAdmin: Boolean
});

const Tasks = mongoose.model('Tasks', TasksSchema);
module.exports = Tasks;