const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const NewsSchema = new Schema ({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    },
    image: {
        type: String
    },
    file: {
        type: String
    }
});

const News = mongoose.model('News', NewsSchema);

module.exports = News;