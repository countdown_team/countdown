const CronJob = require('cron').CronJob;
const moment = require('moment');
const Tasks = require('../models/Tasks');


console.log('Before job instantiation');
module.exports={ job : new CronJob('* * * * *',  function() {
    let date = moment();
    Tasks.find()
        .then(async result => {
            for(let i = 0;i < result.length; i++){
                if( !moment(new Date(result[i].endDate).toISOString()).isAfter(date) && result[i].status !== 'finished'){
                    result[i].status = 'overdue';
                   await Tasks.findOneAndUpdate({_id: result[i]._id}, result[i])
                }
            }
        });
})
};
console.log('After job instantiation');
// job.start();