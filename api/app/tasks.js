const express = require('express');
const Tasks = require('../models/Tasks');
const auth = require('../middleware/auth');
const router = express.Router();
const moment = require('moment');

const createRouter = () => {
  router.post('/', auth, (req, res) => {
    const data = req.body;
    if (req.user.role === 'admin') {
      data.isCreatedByAdmin = true;
    }
    const task = new Tasks(data);
    if (req.user.role === 'admin' || task.user.equals(req.user._id)) {
      task.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error));
    } else {
      res.send('Permission denied!');
    }
  });

  router.put('/:id', auth, async (req, res) => {
    const data = req.body;
    data._id = req.params.id;
    Tasks.findOne({_id: data._id}).then(task => {
      data.categoryName = task.categoryName;
      data.user = task.user;
      if (req.user.role === 'admin' || req.user._id.equals(task.user)) {
        Tasks.findOneAndUpdate({_id: req.params.id}, data)
          .then(result => res.send(result))
          .catch(error => res.status(400).send(error));
      } else {
        res.send({message: "Вы не можете редактировать чужие задачи!"});
      }
    }).catch(error => res.status(400).send(error));
  });

  router.delete('/:id', auth, async (req, res) => {
    const task = await Tasks.findOne({_id: req.params.id});
    if (req.user.role === 'admin') {
      Tasks.deleteOne({_id: req.params.id})
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error));
    } else if (req.user._id.equals(task.user)) {
      Tasks.deleteOne({_id: req.params.id})
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error));
    } else {
      res.send({message: "Вы не можете удалять чужие задачи!"});
    }
  });

  router.get('/', auth, (req, res) => {
    if (req.user.role === 'admin') {
      Tasks.find().populate('categoryName').populate('user')
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error));
    } else {
      Tasks.find({user: req.user._id}).populate('categoryName').populate('user')
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error));
    }
  });

  router.get('/:id', auth, (req, res) => {
    Tasks.findOne({_id: req.params.id}).populate('categoryName').populate('user')
      .then(task => {
        if (req.user.role === 'admin') {
          res.send(task);
        } else if (req.user._id.equals(task.user._id)) {
          res.send(task);
        } else {
          res.send({message: "Вы не можете получать чужие задачи!"});
        }
      }).catch(error => res.status(400).send(error));
  });
  router.get('/:year/:month', auth, (req, res) => {
    let tasks = [];
    Tasks.find()
      .then(async result => {
        if (req.user.role === 'admin') {
          for (let i = 0; i < result.length; i++) {
            let year = await moment(new Date(result[i].startDate).toISOString()).get('year');
            let month = await moment(new Date(result[i].startDate).toISOString()).get('month');
            if (year == req.params.year && month == req.params.month) {
              tasks.push(result[i]);
            }
          }
          res.send(tasks);

        } else {
          res.send({message: "Вы не можете получать чужие задачи!"});
        }
      }).catch(error => res.status(400).send(error));
  });

  return router;
};

module.exports = createRouter;