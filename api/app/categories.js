const express = require('express');
const Categories = require('../models/Categories');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const router = express.Router();

const createRouter = () => {
	router.get('/', (req, res) => {
		Categories.find()
		.then(result => res.send(result))
		.catch(error => res.send(error));
	});
	
	router.get('/:id', [auth, permit('admin')], (req, res) => {
		Categories.findById(req.params.id)
		.then(result => res.send(result))
		.catch(error => res.send(error));
	});
	
	router.post('/', [auth, permit('admin')], (req, res) => {
		const category = new Categories(req.body);
		category.save()
		.then(result => res.send(result))
		.catch(error => res.status(400).send(error))
	});
	
	router.put('/:id', [auth, permit('admin')], (req, res) => {
		const category = req.body;
		category._id = req.params.id;
		
		Categories.findOneAndUpdate({_id: req.params.id}, category)
		.then(result => res.send(result))
		.catch(error => res.status(400).send(error));
	});
	
	router.delete('/:id', [auth, permit('admin')], (req, res) => {
		Categories.deleteOne({_id: req.params.id})
		.then(result => res.send(result))
		.catch(error => res.status(400).send(error));
	});
	
	return router;
};

module.exports = createRouter;
