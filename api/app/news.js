const express = require('express');
const nanoid = require('nanoid');
const multer = require('multer');
const path = require('path');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const News = require('../models/News');
const router = express.Router();

const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});
const upload = multer({storage});

const createRouter = () => {
    router.get('/', async (req, res) => {
        await News.find().sort({date: -1})
            .then(news => res.send(news))
            .catch(() => res.sendStatus(500));
    });


    router.post('/', [auth, permit('admin'), upload.fields([{name: 'image'}, {name: 'file'}])], (req, res) => {
        const newsData = req.body;
        if (req.files && req.files.image) {
            newsData.image = req.files.image[0].filename
        } if (req.files && req.files.file){
            newsData.file = req.files.file[0].filename;
        }

        newsData.date = new Date();
        const newses = new News(newsData);

        newses.save()
            .then(newses => res.send(newses))
            .catch(error => res.status(400).send(error));
    });

    router.get('/:id', (req, res) => {

        News.findById(req.params.id)
            .then(news => res.send(news))
            .catch(() => res.sendStatus(500));
    });

    router.put('/:id', [auth, permit('admin'), upload.fields([{name: 'image'}, {name: 'file'}])], async (req, res) => {
        const newsData = req.body;

        if (req.files.image) {
            newsData.image = req.files.image[0].filename
        }

        if (req.files.file) {
            newsData.file = req.files.file[0].filename;
        }

        News.findOneAndUpdate({_id: req.params.id}, newsData)
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));


    });

    router.delete('/:id', [auth, permit('admin')], async (req, res) => {
        const news = await News.findById(req.params.id);

        if (!news) {
            return res.sendStatus(404);
        }

        news.remove()
            .then(() => res.send('News was deleted!'))
            .catch(error => res.status(400).send(error));
    });

    return router;

};

module.exports = createRouter;

