export default {
	container: {
		paddingTop: 35,
		flex: 1,
		justifyContent: 'center',
		flexDirection: 'column',
		alignItems: 'center'
	},
	sectionHeadingStyle: {
		textAlign: 'center',
		paddingVertical: 5,
		paddingHorizontal: 5
	},
	button: {
		backgroundColor: '#FF9F1C',
		marginVertical: 10,
		width: 250
	}
};