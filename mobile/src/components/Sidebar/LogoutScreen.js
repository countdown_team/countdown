import React, { Component } from "react";
import {ActivityIndicator, Image, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View} from "react-native";
import {connect} from "react-redux";
import {addNews} from "../../store/actions/newses";
import Input from "../../components/Input/Input";
import styles from '../../components/Input/style';
import btnStyle from '../../components/Button/style'
import {logoutUser} from "../../store/actions/user";

class LogoutScreen extends Component {

  componentDidMount() {
    this.props.logout().then(() => {
      this.props.navigation.navigate('Login');
    })
  }

  render() {
    return (
      <View/>
    );
  }
}


const mapStateToProps = state => ({

});

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logoutUser())
});

export default connect(mapStateToProps, mapDispatchToProps)(LogoutScreen);