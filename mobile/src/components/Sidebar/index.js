import React, { Component } from 'react';
import { ScrollView, Text, View } from 'react-native';
import Button from '../Button';
import { NavigationActions } from 'react-navigation';
import PropTypes from 'prop-types';
import styles from './style';

class Sidebar extends Component {
	
	navigateToScreen = (route) => () => {
		const navigate = NavigationActions.navigate({
			routeName: route
		});
		this.props.navigation.dispatch(navigate);
	};
	
	render () {
		return (
			<View style={styles.container}>
				<ScrollView>
					
					<Text style={styles.sectionHeadingStyle}>
						 Sidebar menu
					</Text>
					
					<Button
						icon={{name: 'trash-o', type: 'font-awesome', size: 20}}
						title='Просроченные'
						buttonStyle={styles.button}
						onPress={this.navigateToScreen('Home')}/>
					
					<Button
						icon={{name: 'umbrella', type: 'font-awesome', size: 20}}
						title='Срочные'
						buttonStyle={styles.button}
						onPress={this.navigateToScreen('Info')}/>
					
					<Button
						icon={{name: 'user-circle', type: 'font-awesome', size: 20}}
						title='Общая'
						buttonStyle={styles.button}
						onPress={this.navigateToScreen('Last')}/>
					
					<Button
						icon={{name: 'user-circle', type: 'font-awesome', size: 20}}
						title='Требующие исполнения'
						buttonStyle={styles.button}
						onPress={this.navigateToScreen('Last')}/>
					
					<Button
						icon={{name: 'user-circle', type: 'font-awesome', size: 20}}
						title='Режиме приготовится к исполнению'
						buttonStyle={styles.button}
						onPress={this.navigateToScreen('Last')}/>
					
					<Button
						icon={{name: 'user-circle', type: 'font-awesome', size: 20}}
						title='Календарь'
						buttonStyle={styles.button}
						onPress={this.navigateToScreen('Last')}/>
					<Button
						icon={{name: 'user-circle', type: 'font-awesome', size: 20}}
						title='Добавить новость'
						buttonStyle={styles.button}
						onPress={this.navigateToScreen('News')}/>
          <Button
            icon={{name: 'user-circle', type: 'font-awesome', size: 20}}
            title='Выход'
            buttonStyle={styles.button}
            onPress={this.navigateToScreen('Logout')}/>
				</ScrollView>
			</View>
		);
	}
}

Sidebar.propTypes = {
	navigation: PropTypes.object
};

export default Sidebar;