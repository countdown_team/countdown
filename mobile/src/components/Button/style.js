export default {
	touchable: {
		borderRadius: 5,
		width: 200,
		backgroundColor: '#8d0171'
	},
	button: {
		paddingHorizontal: 5,
		height: 50,
		justifyContent: "center"
	},
	btnText: {
		color: "white",
		fontWeight: "600",
		textAlign: "center",
		fontSize: 14
	}
};