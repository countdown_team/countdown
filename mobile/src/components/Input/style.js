import {Dimensions, StyleSheet} from "react-native";
const {width, height} = Dimensions.get("window");

export default {
  container: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#fff'
	},
	header: {
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: "#8d0171",
		width: width - 80,
		height: 50,
		marginTop: 20,
		borderRadius: 5
	},
	content: {
		flex: 4,
		backgroundColor: "white",
		paddingTop: 20,
		alignItems: "center",
		justifyContent: "flex-start"
	},
	textInput: {
		height: 50,
		borderColor: "#8d0171",
		borderWidth: StyleSheet.hairlineWidth,
		borderRadius: 5,
		marginBottom: 15,
		paddingHorizontal: 15,
		width: width - 80,
		backgroundColor: "#fff",
		fontSize: 14,
		color: "#8d0171"
	},
	text: {
		fontSize: 25,
		color: '#ffffff',
		fontWeight: 'bold'
	},
	textArea: {
		width: width - 80,
		height: 150,
		borderWidth: StyleSheet.hairlineWidth,
		borderRadius: 5,
		borderColor: "#8d0171",
		marginBottom: 15,
		paddingHorizontal: 15,
		textAlignVertical: "top",
		color: "#8d0171",
    justifyContent: "flex-start",
  },
	picker: {
		height: 50,
		width: width - 80
	},
	touchable: {
		width: width - 80,
		height: 60,
		borderRadius: 5,
		backgroundColor: "#8d0171",
		marginBottom: 25,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center'
	},
	checkbox: {
		flexDirection: 'row',
		width: width - 80,
		marginBottom: 25
	},
	pickerContainer: {
		borderWidth: StyleSheet.hairlineWidth,
		borderColor: '#8d0171',
		borderRadius: 5,
		marginBottom: 15
	},
	datePicker: {
		width: width - 80,
        marginBottom: 15
	},
  colors: {
    purple:'#8d0171',
		white: '#fff'
  },
  logo: {
    width: 180,
    height: 80,
    marginTop: 30
  },

  error: {
    fontWeight: 'bold',
    color: 'red',
    fontSize: 20,
    paddingBottom: 20
  }
};