import { NavigationActions } from 'react-navigation';
import { AppNavigator } from '../../navigation/AppNavigator';
import { LOGIN_USER_SUCCESS, BACK } from "../actions/actionTypes";
// Start with two routes: The Main screen, with the Login screen on top.
const firstAction = AppNavigator.router.getActionForPathAndParams('Login');
const tempNavState = AppNavigator.router.getStateForAction(firstAction);
const thirdAction = AppNavigator.router.getActionForPathAndParams('Home');
const initialNavState = AppNavigator.router.getStateForAction(
	firstAction,
	tempNavState,
	thirdAction
);

const reducer = (state = initialNavState, action) => {
	let nextState;
	switch (action.type) {
		case BACK:
			return nextState = AppNavigator.router.getStateForAction(
				NavigationActions.back(),
				state
			);
		case LOGIN_USER_SUCCESS:
			return nextState = AppNavigator.router.getStateForAction(
				NavigationActions.navigate({routeName: 'Home'}),
				state
			);
		default:
			nextState = AppNavigator.router.getStateForAction(action, state);
			break;
	}
	
	// Simply return the original `state` if `nextState` is null or undefined.
	return nextState || state;
};

export default reducer;