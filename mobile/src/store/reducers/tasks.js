import { FETCH_TASKS_SUCCESS, GET_TASKS_BY_DAY } from "../actions/actionTypes";

const initialState = {
	tasks: [],
	currentTasks: [],
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_TASKS_SUCCESS:
			return {...state, tasks: action.tasks};
		case GET_TASKS_BY_DAY:
			const currentTasks = state.tasks.filter(task => {
				return action.date === task.date;
			});
			return {...state, currentTasks: currentTasks};
		default:
			return state;
	}
};

export default reducer;