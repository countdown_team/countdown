import {
    FETCH_CATEGORIES_FAILURE, FETCH_CATEGORIES_SUCCESS, FETCH_USERS_FAILURE,
    FETCH_USERS_SUCCESS, START_REQUEST, CREATE_TASK_FAILURE, CREATE_TASK_SUCCESS
} from "../actions/actionTypes";


const initialState = {
    allCategories: [],
    users: [],
    error: null,
    loading: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case START_REQUEST:
            return {...state, loading: true};
        case FETCH_CATEGORIES_SUCCESS:
            return {...state, allCategories: action.categories};
        case FETCH_CATEGORIES_FAILURE:
            return {...state, error: action.error};
        case FETCH_USERS_SUCCESS:
            return {...state, users: action.users};
        case FETCH_USERS_FAILURE:
            return {...state, error: action.error};
        case CREATE_TASK_SUCCESS:
            return {...state,loading: false};
        case CREATE_TASK_FAILURE:
            return {...state, loading: false};
        default:
            return state;
    }
};

export default reducer;