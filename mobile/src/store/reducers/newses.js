import {FETCH_NEWSES_SUCCESS} from "../actions/actionTypes";

const initialState = {
  newses: [],
  news: [],
  loading: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_NEWSES_SUCCESS:
            return {...state, newses: action.newses};
        default:
            return state;

    }
};

export default reducer;