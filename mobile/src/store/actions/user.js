import axios from '../../axios-api';
import {
  LOGIN_USER_FAILURE,
  LOGIN_USER_SUCCESS, LOGOUT_USER, NEW_USER_FAILURE, NEW_USER_SUCCESS
} from "./actionTypes";


const loginUserSuccess = (user, token) => {
	return {type: LOGIN_USER_SUCCESS, user, token};
};

const loginUserFailure = error => {
	return {type: LOGIN_USER_FAILURE, error};
};

export const logoutUser = () => {
  return (dispatch) => {
    dispatch(logoutExpiredUser());
    return new Promise.resolve()
  }
};

export const logoutExpiredUser = () => {
  return {type: LOGOUT_USER};
};

export const loginUser = userData => {
	return dispatch => {
		return axios.post('/users/sessions', userData).then(
			response => {
				dispatch(loginUserSuccess(response.data.user, response.data.token));
			},
			error => {
				const errorObj = error.response ? error.response.data : {error: 'No internet'};
				dispatch(loginUserFailure(errorObj));
			}
		)
	}
};

export const newUserSuccess = (newUser) => {
	return {type: NEW_USER_SUCCESS, newUser};
};
export const newUserFailure = (error) => {
	return {type: NEW_USER_FAILURE, error};
};



export const addNewUser = (data) => {
	return (dispatch, getState) => {
		return axios.post('/users', data).then(result => {
		    dispatch(newUserSuccess(result.data));
		}, error => {
		    dispatch(newUserFailure(error));
        });
    }
};
