import axios from '../../axios-api';
import { Alert } from 'react-native';
import {
	FETCH_CATEGORIES_FAILURE, FETCH_CATEGORIES_SUCCESS, FETCH_USERS_FAILURE,
	FETCH_USERS_SUCCESS, CREATE_TASK_FAILURE, CREATE_TASK_SUCCESS, START_REQUEST
} from "./actionTypes";

const startRequest = () => {
	return {type: START_REQUEST};
};

const fetchUsersSuccess = users => {
	return {type: FETCH_USERS_SUCCESS, users};
};

const fetchUsersFailure = error => {
	return {type: FETCH_USERS_FAILURE, error};
};

export const fetchUsers = () => {
	return dispatch => {
		axios.get('/users').then(response => {
			dispatch(fetchUsersSuccess(response.data));
		}, error => {
			dispatch(fetchUsersFailure(error));
		})
	};
};

const fetchCategoriesSuccess = categories => {
	return {type: FETCH_CATEGORIES_SUCCESS, categories};
};

const fetchCategoriesFailure = error => {
	return {type: FETCH_CATEGORIES_FAILURE, error};
};

export const fetchCategories = () => {
	return dispatch => {
		axios.get('/categories').then(response => {
			dispatch(fetchCategoriesSuccess(response.data));
		}, error => {
			dispatch(fetchCategoriesFailure(error));
		});
	}
};

const createTaskSuccess = () => {
	return {type: CREATE_TASK_SUCCESS};
};

const createTaskFailure = () => {
	return {type: CREATE_TASK_FAILURE};
};

export const createTask = formData => {
	return dispatch => {
		dispatch(startRequest());
		axios.post('/tasks', formData).then(() => {
			dispatch(createTaskSuccess());
			Alert.alert('Успех', 'Задача успешно создана!');
		}, error => {
			if (error.response.data.message === 'Token incorrect') {
				Alert.alert('Внимание', 'Пожалуйста залогиньтесь обратно!');
			} else if (error.response.data.name === "ValidationError") {
				Alert.alert('Внимание', 'Все поля обязательны к заполнению!');
			}
			console.log(error.response.data);
			dispatch(createTaskFailure());
		});
	}
};

export const addNewCategory = categoryData => {
	return dispatch => {
		axios.post('/categories', categoryData).then(response => {
				Alert.alert('Успех', 'Категория создана!');
			}, error => {
				Alert.alert('Внимание', 'Что-то пошло не так!');
			}
		)
	}
};
