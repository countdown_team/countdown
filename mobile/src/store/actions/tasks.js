import axios from "../../axios-api";
import { FETCH_TASKS_SUCCESS, GET_TASKS_BY_DAY } from "./actionTypes";

export const fetchTasks = () => {
	return dispatch => {
		return axios.get('/tasks').then(
			response => dispatch(fetchTasksSuccess(response.data))
		)
	};
};

const fetchTasksSuccess = tasks => {
	return {type: FETCH_TASKS_SUCCESS, tasks};
};

export const getTasksByDay = date => {
	return {type: GET_TASKS_BY_DAY, date};
};