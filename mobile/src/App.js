import React from "react";
import { Provider } from "react-redux";
import AppWithNavigationState from "./navigation/AppWithNavigationState";
import { persistor, store } from "./store/configureStore";
import { PersistGate } from 'redux-persist/lib/integration/react';
import { ActivityIndicator, StyleSheet, View } from "react-native";

const LoadingView = () => (
	<View style={styles.container}>
		<ActivityIndicator size="large" color="#0000ff"/>
	</View>
);

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "white",
		justifyContent: 'center',
		alignItems: 'center'
	}
});

class App extends React.Component {
	render() {
		return (
			<Provider store={store}>
				<PersistGate loading={<LoadingView />} persistor={persistor}>
					<AppWithNavigationState/>
				</PersistGate>
			</Provider>
		);
	}
}

export default App;
