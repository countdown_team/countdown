import React  from 'react';
import { connect } from 'react-redux';
import { initializeListeners } from 'react-navigation-redux-helpers';
import PropTypes from 'prop-types';
import { navigationPropConstructor } from '../utils/redux';
import { Platform, BackHandler } from "react-native";
import { BACK } from "../store/actions/actionTypes";
import { AppNavigator } from "./AppNavigator";

class AppWithNavigationState extends React.Component {
	static propTypes = {
		dispatch: PropTypes.func.isRequired,
		nav: PropTypes.object.isRequired,
	};
	
	componentWillMount() {
		if (Platform.OS !== 'android') return;
		BackHandler.addEventListener('hardwareBackPress', () => {
			this.props.dispatch({type: BACK});
			return true;
		})
	}
	
	componentWillUnmount() {
		if (Platform.OS === 'android') BackHandler.removeEventListener('hardwareBackPress');
	}
	
	componentDidMount() {
		initializeListeners('root', this.props.nav);
	}
	
	render() {
		const {dispatch, nav} = this.props;
		const navigation = navigationPropConstructor(dispatch, nav);
		return <AppNavigator navigation={navigation}/>;
	}
}

const mapStateToProps = state => ({
	nav: state.nav
});

export default connect(mapStateToProps)(AppWithNavigationState);