import React from "react";
import { createBottomTabNavigator } from "react-navigation";
import Icon from 'react-native-vector-icons/Ionicons';
import SearchScreen from "../screens/SearchScreen";
import NotificationsScreen from "../screens/NotificationsScreen";
import ProfileScreen from "../screens/ProfileScreen";
import SidebarNavigation from "./SidebarNavigation";
import AddTaskScreen from "../screens/AddTaskScreen/AddTaskScreen";
import { appConfig } from "../config";

const TabsNavigation = createBottomTabNavigator(
	{
		Home: {
			screen: SidebarNavigation,
			navigationOptions: {
				tabBarIcon: ({focused}) => (
					<Icon
						name={focused ? "ios-home" : "ios-home-outline"}
						size={30}
						color={"#fff"}
					/>
				)
			}
		},
		AddTask: {
			screen: AddTaskScreen,
			navigationOptions: {
				tabBarIcon: ({focused}) => (
					<Icon
						name={focused ? "ios-add-circle" : "ios-add-circle-outline"}
						size={30}
						color={"#fff"}
					/>
				)
			}
		},
		Search: {
			screen: SearchScreen,
			navigationOptions: {
				tabBarIcon: ({focused}) => (
					<Icon
						name={focused ? "ios-search" : "ios-search-outline"}
						size={30}
						color={"#fff"}
					/>
				)
			}
		},
		Notifications: {
			screen: NotificationsScreen,
			navigationOptions: {
				tabBarIcon: ({focused}) => (
					<Icon
						name={focused ? "ios-heart" : "ios-heart-outline"}
						size={30}
						color={"#fff"}
					/>
				)
			}
		},
		Profile: {
			screen: ProfileScreen,
			navigationOptions: {
				tabBarIcon: ({focused}) => (
					<Icon
						name={focused ? "ios-person" : "ios-person-outline"}
						size={30}
						color={"#fff"}
					/>
				)
			}
		}
	},
	{
		initialRouteName: 'Home',
		tabBarOptions: {
			showLabel: true,
			labelStyle: {
				fontSize: 15,
				color: '#fff'
			},
			style: {
				backgroundColor: appConfig.appMainColor,
			},
		}
	}
);

export default TabsNavigation;