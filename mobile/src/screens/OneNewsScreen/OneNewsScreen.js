import React, {Component} from "react";
import {View, Text, StyleSheet} from "react-native";
import PropTypes from 'prop-types'
import Icon from 'react-native-vector-icons/FontAwesome';
import moment from 'moment';

class OneNewsScreen extends Component {
    render() {
        const date = new Date(this.props.date);
        const formattedDate = moment(date).format("LLL");
        return (
            <View style={styles.content}>
                <View style={styles.words}>
                    <Text style={styles.title}>{this.props.title}</Text>
                    <View style={styles.button}>
                        <Icon
                            name='download'
                            size={25}
                            color='black'
                            style={{height:25,width:25}}/>
                    </View>
                </View>
                <View style={styles.desc}>
                    <Text style={styles.text}>{this.props.description.length > 20 ? `${this.props.description.substr(0, 20)}...` : this.props.description}</Text>
                </View>
                <Text>{formattedDate}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    content: {
        flex: 1,
        width: "80%",
        backgroundColor: "white",
        paddingTop: 20,
        alignItems: "center",
        justifyContent: "flex-start",
        borderColor: "#8d0171",
        borderWidth: StyleSheet.hairlineWidth,
        borderRadius: 5,
        marginBottom: 10
    },
    words: {
        flex: 1,
        flexDirection: 'row'
    },
    title: {
        width: "70%",
        fontSize: 25,
        color: "black",
        fontWeight: "bold",
        borderColor: "black",
    },
    button: {
        width: "25%",
        borderColor: "black",
        alignItems: "flex-end",
    },
    desc: {
       alignItems: "flex-start",
        borderColor: "#8d0171",
        borderWidth: StyleSheet.hairlineWidth,
    },
    text: {
        fontSize: 25,
        color: 'black',
        fontWeight: 'bold',
    },
});
OneNewsScreen.propTypes = {
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired
};

export default OneNewsScreen;
