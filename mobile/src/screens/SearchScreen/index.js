import React, { Component } from "react";
import {
	View,
	Text,
	StyleSheet,
	FlatList,
	ScrollView,
	Dimensions,
	TouchableOpacity,
	Switch,
} from "react-native";
import { getTasksByDay } from "../../store/actions/tasks";
import { connect } from "react-redux";
import moment from 'moment';
import { appConfig, localeConfig } from "../../config";
import autobind from 'autobind-decorator';
import Button from '../../components/Button';
import { CalendarList } from "react-native-calendars";
import { LocaleConfig } from 'react-native-calendars';
import Task from "../../components/Task";

const {width} = Dimensions.get("window");

class SearchScreen extends Component {
	
	componentDidMount() {
		this.inCurrentMonthDays();
		setTimeout(() => this.scrollToTodayDay());
	}
	
	state = {
		today: {
			day: moment().date(),
			month: moment().month() + 1,
			year: moment().year()
		},
		monthView: false,
		days: []
	};
	
	getFullMonth = (currentMonth = this.state.today.month) => {
		if (currentMonth.toString().length === 1) return '0' + currentMonth;
		else return currentMonth.toString();
	};
	
	getFullDate = (day) => {
		const dayNumber = day.toString().length === 1 ? '0' + day : day;
		const monthNumber = this.getFullMonth();
		return `${this.state.today.year}-${monthNumber}-${dayNumber}`
	};
	
	inCurrentMonthDays = (year = this.state.today.year, month = this.state.today.month) => {
		const quantityDays = moment(year + '-' + month, "YYYY-MM").daysInMonth();
		const getWeekDay = weekDay => moment(year + '-' + month + '-' + weekDay.toString(), "YYYY-MM-DD").weekday();
		const days = [];
		for (let i = 1; i <= quantityDays; i++) {
			days.push({
				dayNumber: i,
				weekday: localeConfig.dayNamesShort[getWeekDay(i)],
				date: this.getFullDate(i)
			});
		}
		//this.setState({days});
		return days;
	};
	
	@autobind
	scrollToTodayDay() {
		this.getTasksByDay(this.getFullDate(this.state.today.day));
		if (this.state.monthView) this.calendar.scrollToMonth(`${this.state.today.year}-${this.getFullMonth()}`);
		else this.scrollView.scrollTo({x: this.scrollWidth, animated: true});
	};
	
	scrollToDay = dayNumber => {
		const scrollWidth = dayNumber * appConfig.dayWidth + (dayNumber - 1) * appConfig.marginHorizontalDay - width / 2 - 6;
		if (!this.state.monthView) this.scrollView.scrollTo({x: scrollWidth, animated: true});
		else return false;
	};
	
	getTasksByDay = (date) => this.props.getTasksByDay(date);
	
	@autobind
	async getTasksByDayFromCalendar({dateString, day, month, year}) {
		await this.props.getTasksByDay(dateString);
		await this.changeView(false);
		await this.inCurrentMonthDays(year, this.getFullMonth(month));
		setTimeout(() => this.scrollToDay(day));
	}
	
	@autobind
	renderDay(day) {
		let backgroundColor = appConfig.appMainColor;
		if (day.dayNumber === this.state.today.day) backgroundColor = '#ff0000';
		return (
			<View key={day.dayNumber}>
				<Text style={styles.weekday}>{day.weekday}</Text>
				<TouchableOpacity
					style={[styles.dayContainer, {backgroundColor}]}
					onPress={() => this.getTasksByDay(day.date)}>
					<Text style={styles.dayText}>{day.dayNumber}</Text>
				</TouchableOpacity>
			</View>
		)
	}
	
	@autobind
	renderItem({item}) {
		return <Task {...item}/>;
	};
	
	@autobind
	changeView(value) {
		this.setState({monthView: value});
	}
	
	keyExtractor = (item, index) => item._id;
	
	scrollViewWidth = this.inCurrentMonthDays().length * (appConfig.dayWidth + appConfig.marginHorizontalDay);
	scrollWidth = this.state.today.day * appConfig.dayWidth + (this.state.today.day - 1) * appConfig.marginHorizontalDay - width / 2 - 6;
	
	render() {
		LocaleConfig.locales['ru'] = localeConfig;
		LocaleConfig.defaultLocale = 'ru';
		
		return (
			<View style={styles.container}>
				<View style={styles.topContainer}>
					<Button
						title={localeConfig.today}
						buttonStyle={{width: 100}}
						onPress={this.scrollToTodayDay}/>
					<View style={styles.switchContainer}>
						<Text>{localeConfig.week}</Text>
						<Switch
							onTintColor='#ff0000'
							tintColor='blue'
							thumbTintColor='green'
							value={this.state.monthView}
							onValueChange={this.changeView}/>
						<Text>{localeConfig.month}</Text>
					</View>
				</View>
				{!this.state.monthView ?
					<View style={styles.container}>
						<View style={styles.scrollContainer}>
							<ScrollView
								horizontal
								ref={scrollView => scrollView !== null ? this.scrollView = scrollView : this.scrollView = null}
								contentContainerStyle={[styles.contentContainer, {width: this.scrollViewWidth}]}>
								{this.inCurrentMonthDays().map(this.renderDay)}
							</ScrollView>
						</View>
						<View style={styles.commonContainer}>
							{this.props.currentTasks.length > 0 ?
								<FlatList
									contentContainerStyle={styles.resourcesContainer}
									data={this.props.currentTasks}
									keyExtractor={this.keyExtractor}
									renderItem={this.renderItem}
								/> :
								<View style={styles.resourcesContainer}><Text>{appConfig.emptyDay}</Text></View>
							}
						</View>
					</View>
					:
					<View>
						<CalendarList
							ref={(calendar) => this.calendar = calendar}
							futureScrollRange={this.state.today.month + 11}
							scrollEnabled={true}
							showScrollIndicator={true}
							firstDay={1}
							pastScrollRange={this.state.today.month}
							pagingEnabled={true}
							onDayPress={this.getTasksByDayFromCalendar}
						/>
					</View>
				}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	commonContainer: {
		flex: 1,
	},
	container: {
		backgroundColor: 'rgb(241,241,242)',
		flex: 1,
		flexDirection: 'column'
	},
	resourcesContainer: {
		backgroundColor: 'white',
		paddingHorizontal: 20,
		flexGrow: 1
	},
	contentContainer: {
		paddingVertical: 20,
		justifyContent: 'space-around',
		backgroundColor: '#ffffff'
	},
	dayContainer: {
		width: appConfig.dayWidth,
		height: appConfig.dayHeight,
		borderRadius: 60,
		alignItems: 'center',
		justifyContent: 'center'
	},
	weekday: {
		textAlign: 'center',
		marginBottom: 5
	},
	dayText: {
		color: 'white',
		fontSize: 16,
		fontWeight: 'bold'
	},
	topContainer: {
		justifyContent: 'space-between',
		flexDirection: 'row',
		alignItems: 'center',
		paddingHorizontal: 20,
		paddingVertical: 20,
		backgroundColor: '#ffffff'
	},
	scrollContainer: {
		backgroundColor: '#ffffff'
	},
	switchContainer: {
		flexDirection: 'row',
		alignItems: 'center'
	}
});

const mapStateToProps = state => ({
	currentTasks: state.tasks.currentTasks
});

const mapDispatchToProps = dispatch => ({
	getTasksByDay: date => dispatch(getTasksByDay(date))
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchScreen);
