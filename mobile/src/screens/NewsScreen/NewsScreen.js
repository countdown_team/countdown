import React, { Component } from "react";
import { View, Text, StyleSheet, ScrollView } from "react-native";
import OneNewsScreen from "../OneNewsScreen/OneNewsScreen";
import { connect } from "react-redux";
import { fetchNewses } from "../../store/actions/newses";
import styles from '../../components/Input/style';

class NewsScreen extends Component {
	componentDidMount() {
		this.props.fetchNewses();
	}
	
	render() {
		return (
			<ScrollView style={{backgroundColor: "#fff"}}>
				<View style={styles.container}>
					<View style={styles.header}>
						<Text style={styles.text}>All news</Text>
					</View>
					<View style={style.content}>
						{this.props.newses.map(news => (
							<OneNewsScreen
								key={news._id}
								id={news._id}
								title={news.title}
								description={news.description}
								date={news.date}
							/>
						))}
					</View>
				</View>
			</ScrollView>
		);
	}
}

const style = StyleSheet.create({
	content: {
		flex: 1,
		width: "100%",
		backgroundColor: "white",
		paddingTop: 50,
		alignItems: "center",
		justifyContent: "flex-start"
	}
});


const mapStateToProps = state => ({
	newses: state.newses.newses
});

const mapDispatchToProps = dispatch => ({
	fetchNewses: () => dispatch(fetchNewses())
});

export default connect(mapStateToProps, mapDispatchToProps)(NewsScreen);
