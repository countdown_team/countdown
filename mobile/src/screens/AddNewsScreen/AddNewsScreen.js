import React, { Component } from "react";
import {ActivityIndicator, Image, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View} from "react-native";
import {connect} from "react-redux";
import {addNews} from "../../store/actions/newses";
import Input from "../../components/Input/Input";
import styles from '../../components/Input/style';
import btnStyle from '../../components/Button/style'

class AddNewsScreen extends Component {

    state = {
        title: '',
        description: '',
    };

    submitNewsFormHandler = (event) => {
        event.preventDefault();
        this.props.addNews(this.state);
    };

    changeNewsTitle = (text) => {
        this.setState({title: text})
    };

    changeNewsDescription = (text) => {
        this.setState({description: text})
    };


    render() {
        return (
            <ScrollView style={{backgroundColor: styles.colors.white}}>
                <View style={styles.container}>
                    <View style={styles.header}>
                        <Text style={styles.text}>Create your news</Text>
                    </View>
                    <View style={style.content}>
                        <Input
                            placeholder={"news title"}
                            style={styles.textInput}
                            value={this.state.title}
                            onChangeText={this.changeNewsTitle}
                        />
                        <Input
                            style={styles.textArea}
                            underlineColorAndroid="transparent"
                            placeholder={"news description"}
                            numberOfLines={10}
                            multiline={true}
                            value={this.state.description}
                            onChangeText={this.changeNewsDescription}
                        />
                        <TouchableOpacity style={styles.touchable} onPressOut={this.submitNewsFormHandler}>
                            <View style={btnStyle.button}>
                                <Text style={btnStyle.btnText}>Add</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>

        );
    }
}

const style = StyleSheet.create({
    content: {
        flex: 1,
        width: "100%",
        backgroundColor: "white",
        paddingTop: 50,
        alignItems: "center",
        justifyContent: "flex-start"
    }
});

const mapStateToProps = state => ({
    newses: state.newses.newses
});

const mapDispatchToProps = dispatch => ({
    addNews: (newsData) => dispatch(addNews(newsData))
});

export default connect(mapStateToProps, mapDispatchToProps)(AddNewsScreen);