import React, { Component } from "react";
import {View, Text, StyleSheet, TouchableOpacity, ActivityIndicator} from "react-native";
import {addNewUser} from "../../store/actions/user";
import {connect} from "react-redux";
import styles from '../../components/Input/style';
import btnStyle from '../../components/Button/style';
import Input from "../../components/Input/Input";


class NotificationsScreen extends Component {

	state = {
		username: '',
		password: '',
		role: ''
	};
    submitUserFormHandler = (event) => {
        event.preventDefault();
        this.props.addNewUser(this.state);
	};
    changeUserName = text => {
        this.setState({username: text});
    };
    changeThisUserPassword = text => {
        this.setState({password: text});
    };
    changeThisUserRole = text => {
    	this.setState({role: text})
	};
	render() {
		return (
			<View style={styles.container}>
        <View style={styles.header}>
            <Text style={styles.text}>Add new user</Text>
        </View>
				<View style={style.content}>
            <Input
                placeholder={"username"}
                style={styles.textInput}
                value={this.state.username}
                onChangeText={this.changeUserName}
            />
            <Input
                placeholder={"password"}
                style={styles.textInput}
                secureTextEntry={true}
                value={this.state.password}
                onChangeText={this.changeThisUserPassword}
                returnKeyType={"send"}
            />
            <Input
              placeholder={"role"}
              style={styles.textInput}
              value={this.state.role}
              onChangeText={this.changeThisUserRole}
            />
            <TouchableOpacity style={style.touchable} onPressOut={this.submitUserFormHandler}>
              <View style={btnStyle.button}>
                <Text style={btnStyle.btnText}>Add</Text>
              </View>
            </TouchableOpacity>
				</View>
			</View>
		);
	}
}

const style = StyleSheet.create({
    content: {
        flex: 1,
		    width: "100%",
        backgroundColor: "white",
        paddingTop: 50,
        alignItems: "center",
        justifyContent: "flex-start"
    },
    touchable: {
        borderRadius: 5,
        backgroundColor: "#8d0171",
        width: "80%",
        marginTop: 25
    }
});
const mapStateToProps = state => ({
    newUser: state.newUser
});

const mapDispatchToProps = dispatch => ({
    addNewUser: data => dispatch(addNewUser(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(NotificationsScreen);
