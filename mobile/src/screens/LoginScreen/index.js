import React from "react";
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { loginUser } from "../../store/actions/user";
import Button from '../../components/Button';
import {
	View,
	Text,
	StyleSheet,
	Dimensions,
	TextInput,
} from "react-native";
import Input from "../../components/Input/Input";

const {width, height} = Dimensions.get("window");


class LoginScreen extends React.Component {
	state = {
		username: '',
		password: ''
	};
	
	submitFormHandler = () => {
		this.props.loginUser(this.state);
	};
	
	changeUsername = text => {
		this.setState({username: text});
	};
	
	changePassword = text => {
		this.setState({password: text});
	};
	
	render() {
		return (
			<View style={styles.container}>
				<View style={styles.header}>
					<Text style={styles.text}>Login screen</Text>
				</View>
				<View style={styles.content}>
					{this.props.loginError && <Text style={styles.error}>{this.props.loginError.error}</Text>}
					<Input
						placeholder={"Username"}
						style={styles.textInput}
						value={this.state.username}
						onChangeText={this.changeUsername}
					/>
					<TextInput
						placeholder={"Password"}
						style={styles.textInput}
						autoCapitalize="none"
						secureTextEntry={true}
						value={this.state.password}
						onChangeText={this.changePassword}
						returnKeyType={"send"}
						onSubmitEditing={this.submitFormHandler}
					/>
					<Button
						title='Login'
						buttonStyle={styles.touchable}
						onPress={this.submitFormHandler}/>
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	header: {
		flex: 1,
		backgroundColor: "#8d0171",
		alignItems: "center",
		justifyContent: "center",
		width
	},
	logo: {
		width: 180,
		height: 80,
		marginTop: 30
	},
	content: {
		flex: 4,
		backgroundColor: "white",
		paddingTop: 50,
		alignItems: "center",
		justifyContent: "flex-start"
	},
	textInput: {
		height: 50,
		borderColor: "#bbb",
		borderWidth: StyleSheet.hairlineWidth,
		width: width - 80,
		borderRadius: 5,
		marginBottom: 15,
		paddingHorizontal: 15,
		backgroundColor: "#FAFAFA",
		fontSize: 14
	},
	touchable: {
		borderRadius: 5,
		backgroundColor: "#8d0171",
		width: width - 80,
		marginTop: 25
	},
	text: {
		fontSize: 30,
		color: '#ffffff',
		fontWeight: 'bold'
	},
	error: {
		fontWeight: 'bold',
		color: 'red',
		fontSize: 20,
		paddingBottom: 20
	}
});

LoginScreen.propTypes = {
	navigation: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
	loginError: state.user.loginError
});

const mapDispatchToProps = dispatch => ({
	loginUser: userData => dispatch(loginUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
