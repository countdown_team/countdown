export default {
	apiUrl: 'http://192.168.1.100:8000/',
};

export const appConfig = {
	dayWidth: 35,
	dayHeight: 35,
	marginHorizontalDay: 24,
	appMainColor: '#8d0171',
	taskBackgroundColor: '#ffffff',
	taskBorderColor: '#000000',
	emptyDay: 'На эту дату нет задач ))',
};

export const localeConfig = {
	monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сеньтябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
	monthNamesShort: ['Янв.', 'Фев.', 'Мар.', 'Апр.', 'Май', 'Июн.', 'Июл.', 'Авг.', 'Сен.', 'Окт.', 'Нов.', 'Дек.'],
	dayNames: ['Воскресение', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
	dayNamesShort: ['Вс.', 'Пн.', 'Вт.', 'Ср.', 'Чт.', 'Пт.', 'Сб.'],
	today: 'Сегодня',
	month: 'Месяц',
	week: 'Неделя'
};